# README #

This project started as I needed for my work an application to proper handle vacation requests for a larger team.
It provides a simple mechanism for persons to request vacation and get them granted by their responsible managers.

The manager can get various overviews which support them in their planning

### How do I get set up? ###

In order to get started you need to clone the project, have a database set up (tested currently on postgresql) and build via gradle