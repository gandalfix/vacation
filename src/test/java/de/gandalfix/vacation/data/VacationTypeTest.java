package de.gandalfix.vacation.data;

import de.gandalfix.vacation.domain.data.entities.VacationType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class VacationTypeTest {

    @Test
    public void testFromAbbreviationOk() throws Exception {
        assertEquals(VacationType.OVER_HOURS, VacationType.fromAbbreviation("O"));
        assertEquals(VacationType.SPECIAL_VACATION, VacationType.fromAbbreviation("S"));
        assertEquals(VacationType.VACATION, VacationType.fromAbbreviation("V"));
        assertEquals(VacationType.TRAINING, VacationType.fromAbbreviation("T"));
    }

    @Test
    public void testFromAbbreviationNull() throws Exception {
        assertNull(VacationType.fromAbbreviation(null));
        assertNull(VacationType.fromAbbreviation(""));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromAbbreviationFail1() throws Exception {
        VacationType.fromAbbreviation("A");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromAbbreviationFail2() throws Exception {
        VacationType.fromAbbreviation("ABC");
    }
}