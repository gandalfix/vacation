package de.gandalfix.vacation.datageneration;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class DataGenerationUtil {

    public static LocalDate findFirstFriday(LocalDate localDate) {
        // Set to start of month
        localDate = localDate.withDayOfMonth(1);
        int diff = DayOfWeek.FRIDAY.getValue() - localDate.getDayOfWeek().getValue();
        if (diff < 0) diff = diff + 7;
        localDate = localDate.plusDays(diff);
        return localDate;
    }
}
