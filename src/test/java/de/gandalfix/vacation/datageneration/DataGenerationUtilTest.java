package de.gandalfix.vacation.datageneration;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;

import static org.junit.Assert.*;

@RunWith(DataProviderRunner.class)
public class DataGenerationUtilTest {

    @DataProvider
    public static Object[][] dates() {
        return new Object[][] {
                {LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 2)},
                {LocalDate.of(2015, 2, 1), LocalDate.of(2015, 2, 6)},
                {LocalDate.of(2015, 2, 3), LocalDate.of(2015, 2, 6)},
                {LocalDate.of(2015, 4, 1), LocalDate.of(2015, 4, 3)},
        };
    }

    @Test
    @UseDataProvider("dates")
    public void testFindFirstFirstFriday(LocalDate startDate, LocalDate expected) {
        Assert.assertEquals(expected, DataGenerationUtil.findFirstFriday(startDate));
    }
}