package de.gandalfix.vacation.datageneration;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Data generation class.
 * This is specialized for my usage pattern.
 * Uses postgresql specific function to create UUIDs
 */
public class DataGeneration {

    public static void main(String[] args) {
        generateSecondAndFourthFridayProjectOffForGroup("_SPECIALGROUPNAME_");
    }

    private static void generateSecondAndFourthFridayProjectOffForGroup(String groupName) {
        LocalDate currentDate = LocalDate.of(2015, 1, 1);

        System.out.println("CREATE SEQUENCE vacationSeq START 101;");

        while (currentDate.getYear() < 2016) {
            LocalDate firstFriday = DataGenerationUtil.findFirstFriday(currentDate);
            LocalDate secondFriday = firstFriday.plusWeeks(1);
            LocalDate fourthFriday = firstFriday.plusWeeks(3);

            System.out.println(printVacationSql(secondFriday, groupName));
            System.out.println(printVacationSql(fourthFriday, groupName));

            currentDate = currentDate.plusMonths(1);
        }

        System.out.println("DROP SEQUENCE vacationSeq;");
    }

    private static String printVacationSql(LocalDate date, String group) {
        String sql = "insert into vacation (id, change_date, date, status, type, person_id) " +
                " select " + "uuid_in(md5(nextval('vacationSeq')::text)::cstring), now(), '" + date + "', 'GRANTED', 'PROJECT_OFF', person_id " +
                "from person_group where group_id = uuid_in(md5('" + group + "'::text)::cstring)::text;";
        return sql;
    }
}
