package de.gandalfix.vacation.util;

import com.google.common.collect.Sets;
import de.gandalfix.vacation.application.webapp.data.VacationData;
import de.gandalfix.vacation.domain.data.entities.*;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class VacationUtilTest {


    @Test
    public void testCreateShallowCopy() throws Exception {
        Vacation v = new Vacation();
        v.setPerson(new Person());
        v.setType(VacationType.OVER_HOURS);
        v.setStatus(VacationStatus.GRANTED);
        v.setDate(LocalDate.now());

        Set<Vacation> vs = Sets.newHashSet(v);

        Set<VacationData> copy = VacationUtil.vacationsToWebData(vs);

        assertEquals(vs.size(), copy.size());
        VacationData vcopy = copy.iterator().next();
        assertEquals(v.getDate(), vcopy.date);
        assertEquals(v.getStatus(), vcopy.status);
        assertEquals(v.getType(), vcopy.type);
    }

    @Test
    public void testGetVacationLeftPerYear() {
        Person person = new Person();
        person.setVacationPerYear(new HashSet<>());
        person.setVacations(new HashSet<>());

        person.getVacationPerYear().add(new VacationPerYear(person, 2015, 24));
        person.getVacations().add(createVacation(person, LocalDate.now(), VacationStatus.SUBMITTED, VacationType.VACATION));
        person.getVacations().add(createVacation(person, LocalDate.now().plusDays(10), VacationStatus.SUBMITTED, VacationType.VACATION));
        person.getVacations().add(createVacation(person, LocalDate.now(), VacationStatus.SUBMITTED, VacationType.PROJECT_OFF));

        Map<Integer, Integer> vacationsPerYear = VacationUtil.getVacationLeftPerYear(person);

        assertEquals(vacationsPerYear.size(), 1);
        assertEquals(Integer.valueOf(8), vacationsPerYear.get(Integer.valueOf(2015)));
    }

    private Vacation createVacation(Person person, LocalDate date, VacationStatus status, VacationType type) {
        Vacation vacation = new Vacation(person, date, status, type);
        vacation.setId(UUID.randomUUID().toString());
        return vacation;
    }
}
