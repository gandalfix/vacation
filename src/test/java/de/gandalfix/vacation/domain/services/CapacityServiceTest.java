package de.gandalfix.vacation.domain.services;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import de.gandalfix.vacation.domain.data.entities.Vacation;
import de.gandalfix.vacation.domain.data.entities.VacationStatus;
import de.gandalfix.vacation.domain.data.entities.VacationType;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(DataProviderRunner.class)
public class CapacityServiceTest {

    public static LocalDate from = LocalDate.of(2014,1,1);
    public static LocalDate to = LocalDate.of(2014,12,31);

    @DataProvider
    public static Object[][] vacationStatusToKeep() {
        return new Object[][] {
                {VacationStatus.SUBMITTED},
                {VacationStatus.GRANTED},
        };
    }

    @DataProvider
    public static Object[][] vacationTypeToKeepAlways() {
        return new Object[][] {
                {VacationType.SPECIAL_VACATION},
                {VacationType.VACATION},
                {VacationType.OVER_HOURS},
                {VacationType.TRAINING},
        };
    }

    @Test
    @UseDataProvider("vacationStatusToKeep")
    public void testFilterVacationsPlannedKeep(VacationStatus status) throws Exception {
        List<Vacation> l = new ArrayList<>();
        l.add(vacation(status, VacationType.VACATION));
        l = new CapacityService().filterVacations(true, from, to, l);
        assertEquals(1, l.size());
    }

    @Test
    public void testFilterVacationsPlannedToRemove() throws Exception {
        List<Vacation> l = new ArrayList<>();
        l.add(vacation(VacationStatus.PLANNED, VacationType.VACATION));
        l = new CapacityService().filterVacations(true, from, to, l);
        assertEquals(0, l.size());
    }

    @Test
    @UseDataProvider("vacationTypeToKeepAlways")
    public void testFilterVacationsTypeToKeepWithProjectOff(VacationType type) throws Exception {
        filterRealVacation(type, true);
    }

    @Test
    @UseDataProvider("vacationTypeToKeepAlways")
    public void testFilterVacationsTypeToKeepWithoutProjectOff(VacationType type) throws Exception {
        filterRealVacation(type, false);
    }

    @Test
    public void testFilterProjectOff() {
        List<Vacation> l = new ArrayList<>();
        l.add(vacation(VacationStatus.SUBMITTED, VacationType.PROJECT_OFF));
        l = new CapacityService().filterVacations(true, from, to, l);
        assertEquals(1, l.size());
    }

    @Test
    public void testNotFilterProjectOff() {
        List<Vacation> l = new ArrayList<>();
        l.add(vacation(VacationStatus.SUBMITTED, VacationType.PROJECT_OFF));
        l = new CapacityService().filterVacations(false, from, to, l);
        assertEquals(0, l.size());
    }

    @Test
    public void testFilterVacationsByDate() throws Exception {
        List<Vacation> l = new ArrayList<>();
        Vacation vacation = vacation(VacationStatus.GRANTED, VacationType.VACATION);
        vacation.setDate(LocalDate.of(2015, 1, 1));
        l.add(vacation);
        l = new CapacityService().filterVacations(true, from, to, l);
        assertEquals(0, l.size());
    }

    private void filterRealVacation(VacationType type, boolean projectOffIsNotInCapacity) {
        List<Vacation> l = new ArrayList<>();
        l.add(vacation(VacationStatus.SUBMITTED, type));
        l = new CapacityService().filterVacations(projectOffIsNotInCapacity, from, to, l);
        assertEquals(1, l.size());
    }

    private Vacation vacation(VacationStatus status, VacationType type) {
        Vacation v = new Vacation();
        v.setStatus(status);
        v.setType(type);
        v.setDate(LocalDate.of(2014,2,2));
        return v;
    }
}