package de.gandalfix.vacation.domain.repositories;

import de.gandalfix.vacation.domain.data.entities.CalendarDate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CalendarDateRepository extends CrudRepository<CalendarDate, String> {

    List<CalendarDate> findByDateBetween(LocalDate start, LocalDate end);
}
