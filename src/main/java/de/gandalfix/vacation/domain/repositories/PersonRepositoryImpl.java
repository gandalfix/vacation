package de.gandalfix.vacation.domain.repositories;

import de.gandalfix.vacation.domain.data.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class PersonRepositoryImpl implements PersonRepositoryCustom {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Person> findPersonsWithoutUser() {
        TypedQuery query = entityManager.createQuery("from Person where id not in (select person from User) order by lastName, firstName", Person.class);
        return query.getResultList();
    }

    public List<Person> findPersonBySearchString(String searchString) {
        searchString = "%" + searchString + "%";
        TypedQuery query = entityManager.createQuery("from Person where lower(lastName) like lower(?) or lower(firstName) like lower(?) order by lastName, firstName", Person.class  );
        query.setParameter(1, searchString);
        query.setParameter(2, searchString);
        return query.getResultList();
    }
}
