package de.gandalfix.vacation.domain.repositories;

import de.gandalfix.vacation.domain.data.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    User findByLogin(String login);
}
