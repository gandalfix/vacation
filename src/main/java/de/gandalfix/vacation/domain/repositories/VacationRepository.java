package de.gandalfix.vacation.domain.repositories;

import de.gandalfix.vacation.domain.data.entities.Person;
import de.gandalfix.vacation.domain.data.entities.Vacation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface VacationRepository extends CrudRepository<Vacation, String> {

    List<Vacation> findByPersonAndDate(Person person, LocalDate date);
    List<Vacation> findByPerson(Person person);
}
