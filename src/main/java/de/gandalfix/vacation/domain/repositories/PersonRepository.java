package de.gandalfix.vacation.domain.repositories;

import de.gandalfix.vacation.domain.data.entities.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, String>, PersonRepositoryCustom {
}
