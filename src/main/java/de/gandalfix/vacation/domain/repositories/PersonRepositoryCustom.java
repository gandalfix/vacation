package de.gandalfix.vacation.domain.repositories;

import de.gandalfix.vacation.domain.data.entities.Person;

import java.util.List;

public interface PersonRepositoryCustom {

    List<Person> findPersonsWithoutUser();

    List<Person> findPersonBySearchString(String string);
}
