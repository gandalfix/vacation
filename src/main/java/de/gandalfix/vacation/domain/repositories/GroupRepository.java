package de.gandalfix.vacation.domain.repositories;

import de.gandalfix.vacation.domain.data.entities.Group;
import de.gandalfix.vacation.domain.data.entities.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, String> {

    List<Group> findByProject(Project project);
}
