package de.gandalfix.vacation.domain.repositories;

import de.gandalfix.vacation.domain.data.entities.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends CrudRepository<Project, String> {

}
