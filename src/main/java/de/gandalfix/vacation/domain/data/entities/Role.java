package de.gandalfix.vacation.domain.data.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "roles")
public class Role extends BaseEntity {

    @NotNull
    private String name;

    @ManyToOne()
    @JoinColumn(name="PROJECT_ID")
    private Project project;

    @ManyToMany(mappedBy="roles")
    Set<User> users;

    @Column(name = "can_grant")
    private boolean grant;

    private boolean teamCalendar;

    private boolean projectAdmin;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public boolean isGrant() {
        return grant;
    }

    public void setGrant(boolean grant) {
        this.grant = grant;
    }

    public boolean isTeamCalendar() {
        return teamCalendar;
    }

    public void setTeamCalendar(boolean teamCalendar) {
        this.teamCalendar = teamCalendar;
    }

    public boolean isProjectAdmin() {
        return projectAdmin;
    }

    public void setProjectAdmin(boolean projectAdmin) {
        this.projectAdmin = projectAdmin;
    }
}
