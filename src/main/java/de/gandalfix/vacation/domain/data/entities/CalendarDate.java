package de.gandalfix.vacation.domain.data.entities;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * CalendarDate is an entry within the calender.
 * It can mark whether is it a bank holiday or a half work day.
 */
@Entity
public class CalendarDate extends BaseEntity {

    private String name;
    @NotNull
    private LocalDate date;
    private boolean bankHoliday;
    private boolean halfDay;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isBankHoliday() {
        return bankHoliday;
    }

    public void setBankHoliday(boolean bankHoliday) {
        this.bankHoliday = bankHoliday;
    }

    public boolean isHalfDay() {
        return halfDay;
    }

    public void setHalfDay(boolean halfDay) {
        this.halfDay = halfDay;
    }
}
