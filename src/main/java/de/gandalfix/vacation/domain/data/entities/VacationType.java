package de.gandalfix.vacation.domain.data.entities;

import com.google.common.base.Strings;

public enum VacationType {
    VACATION("V", 1),
    HALF_DAY_VACATION("H", 0.5),
    TRAINING("T", 0),
    SPECIAL_VACATION("S", 0),
    OVER_HOURS("O", 0),
    PROJECT_OFF("P", 0, false),
    NO_VACATION("", 0, false);

    private final String abbreviation;
    private final boolean isVacation;
    private final double multiplicator;

    VacationType(String abbreviation, double multiplicator) {
        this.abbreviation = abbreviation;
        this.multiplicator = multiplicator;
        this.isVacation = true;
    }

    VacationType(String abbreviation, double multiplicator, boolean isVacation) {
        this.abbreviation = abbreviation;
        this.isVacation = isVacation;
        this.multiplicator = multiplicator;
    }

    /**
     * Converts abbreviation into a full VacationType.
     * @param abbreviation may be null
     * @return VacationType or null if null or empty string is passed into
     * @throws java.lang.IllegalArgumentException in case of unknown abbreviation
     */
    public static VacationType fromAbbreviation(String abbreviation) {
        if (Strings.isNullOrEmpty(abbreviation)) return null;

        for (VacationType t : values()) {
            if (t.abbreviation.equals(abbreviation)) {
                return t;
            }
        }

        throw new IllegalArgumentException(String.format("Unknown abbreviation type '%s'", abbreviation));
    }

    public double getWorkingHours(int dayLengthInHours) {
        return this.multiplicator * dayLengthInHours;
    }
}
