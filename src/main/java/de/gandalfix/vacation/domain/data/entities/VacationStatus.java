package de.gandalfix.vacation.domain.data.entities;

public enum VacationStatus {
    PLANNED, SUBMITTED, GRANTED;
}
