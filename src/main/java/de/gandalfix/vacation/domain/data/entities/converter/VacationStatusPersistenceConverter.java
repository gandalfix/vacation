package de.gandalfix.vacation.domain.data.entities.converter;

import de.gandalfix.vacation.domain.data.entities.VacationStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Converter to automatically persist LocalDateTime.
 */
@Converter(autoApply = true)
public class VacationStatusPersistenceConverter implements AttributeConverter<VacationStatus, String> {

    @Override
    public String convertToDatabaseColumn(VacationStatus entityValue) {
        return entityValue.name();
    }

    @Override
    public VacationStatus convertToEntityAttribute(String databaseValue) {
        return VacationStatus.valueOf(databaseValue);
    }
}