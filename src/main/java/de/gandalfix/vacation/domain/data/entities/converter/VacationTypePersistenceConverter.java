package de.gandalfix.vacation.domain.data.entities.converter;

import de.gandalfix.vacation.domain.data.entities.VacationType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Converter to automatically persist LocalDateTime.
 */
@Converter(autoApply = true)
public class VacationTypePersistenceConverter implements AttributeConverter<VacationType, String> {

    @Override
    public String convertToDatabaseColumn(VacationType entityValue) {
        return entityValue.name();
    }

    @Override
    public VacationType convertToEntityAttribute(String databaseValue) {
        return VacationType.valueOf(databaseValue);
    }
}