package de.gandalfix.vacation.domain.data.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Group groups persons.
 * For example this is the representation of teams.
 */
@Entity
@Table(name = "groups")
public class Group extends BaseEntity {

    @NotNull
    private String name;

    @ManyToOne()
    @JoinColumn(name="PROJECT_ID")
    private Project project;

    @ManyToMany(mappedBy="groups")
    private Set<Person> members;

    public void add(Person person) {
        members.add(person);
        person.getGroups().add(this);
    }

    public void remove(Person person) {
        members.remove(person);
        person.getGroups().remove(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Person> getMembers() {
        return members;
    }

    public void setMembers(Set<Person> members) {
        this.members = members;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
