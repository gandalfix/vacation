package de.gandalfix.vacation.domain.data.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Vacation entry for a person.
 */
@Entity
public class Vacation extends BaseEntity {

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="PERSON_ID")
    private Person person;
    @NotNull
    private LocalDate date;
    @NotNull
    private VacationStatus status;
    @NotNull
    private VacationType type;

    public Vacation() {};

    public Vacation(Person person, LocalDate date, VacationStatus status, VacationType type) {
        this.person = person;
        this.date = date;
        this.status = status;
        this.type = type;
    }

    /**
     * Creates a copy of this instance, without copying the person attribute and the attributes of the BaseEntity.
     * @return new instances.
     */
    public Vacation shallowCopy() {
        Vacation copy = new Vacation();
        copy.setDate(this.getDate());
        copy.setStatus(this.getStatus());
        copy.setType(this.getType());
        return copy;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public VacationStatus getStatus() {
        return status;
    }

    public void setStatus(VacationStatus status) {
        this.status = status;
    }

    public VacationType getType() {
        return type;
    }

    public void setType(VacationType type) {
        this.type = type;
    }
}
