package de.gandalfix.vacation.domain.data.entities;

import com.google.common.base.Strings;

import javax.persistence.*;
import java.util.Set;

/**
 * Person represents a person, that is able to have vacation.
 */
@Entity
public class Person extends BaseEntity {

    private String title;
    private String firstName;
    private String lastName;
    private String code;
    private String email;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "person")
    private Set<VacationPerYear> vacationPerYear;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "person")
    private Set<Vacation> vacations;

    @ManyToMany
    @JoinTable(
            name="PERSON_GROUP",
            joinColumns={@JoinColumn(name="person_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="group_id", referencedColumnName="id")})
    private Set<Group> groups;

    @ManyToMany
    @JoinTable(
            name="PERSON_PROJECT",
            joinColumns={@JoinColumn(name="person_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="project_id", referencedColumnName="id")})
    private Set<Project> projects;

    public String getFullName() {
        String fullName = "";
        fullName += lastName + "," + firstName;
        if (!Strings.isNullOrEmpty(code)) {
            fullName += " (" + code + ")";
        }
        return fullName;
    }

    public void add(Group group) {
        groups.add(group);
        group.getMembers().add(this);
    }

    public void remove(Group group) {
        groups.remove(group);
        group.getMembers().remove(this);
    }

    public void add(Vacation vacation) {
        vacations.add(vacation);
    }

    public void remove(Vacation vacation) {
        vacations.remove(vacation);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<VacationPerYear> getVacationPerYear() {
        return vacationPerYear;
    }

    public void setVacationPerYear(Set<VacationPerYear> vacationPerYear) {
        this.vacationPerYear = vacationPerYear;
    }

    public Set<Vacation> getVacations() {
        return vacations;
    }

    public void setVacations(Set<Vacation> vacations) {
        this.vacations = vacations;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
