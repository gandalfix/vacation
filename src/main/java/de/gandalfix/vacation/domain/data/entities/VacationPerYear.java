package de.gandalfix.vacation.domain.data.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Amount of vacation a person has per year.
 * This is not in days as we want to stick to int values, but there are half days of vacation.
 */
@Entity
public class VacationPerYear extends BaseEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="PERSON_ID")
    private Person person;
    private int year;
    private int vacationHours;

    public VacationPerYear() {

    }

    public VacationPerYear(Person person, int year, int vacationHours) {
        this.person = person;
        this.year = year;
        this.vacationHours = vacationHours;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getVacationHours() {
        return vacationHours;
    }

    public void setVacationHours(int vacationHours) {
        this.vacationHours = vacationHours;
    }
}
