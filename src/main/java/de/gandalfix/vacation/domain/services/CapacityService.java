package de.gandalfix.vacation.domain.services;

import de.gandalfix.vacation.domain.data.entities.*;
import de.gandalfix.vacation.domain.repositories.CalendarDateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service to work with capacity.
 * Capacity in this case can be understood as number of calendar days within a time frame minus various absences like
 * weekends, bank holidays, vacations, ...
 */
@Service
public class CapacityService {

    Logger log = LoggerFactory.getLogger(CapacityService.class);

    @Autowired
    private CalendarDateRepository calendarDateRepository;

    public static class CapacityResult {
        public final Group group;
        public final LocalDate from;
        public final LocalDate to;
        public final boolean projectOffIsNotInCapacity;
        public final long weekendCount;
        public final long bankHolidays;
        public final long vacationDays;
        public final long groupCount;
        public final long totalDays;
        public final long capacity;

        public CapacityResult(
                Group group,
                LocalDate from,
                LocalDate to,
                boolean projectOffIsNotInCapacity,
                long weekendCount,
                long bankHolidays,
                long vacationDays,
                long groupCount,
                long totalDays,
                long capacity) {
            this.group = group;
            this.from = from;
            this.to = to;
            this.projectOffIsNotInCapacity = projectOffIsNotInCapacity;
            this.weekendCount = weekendCount;
            this.bankHolidays = bankHolidays;
            this.vacationDays = vacationDays;
            this.groupCount = groupCount;
            this.totalDays = totalDays;
            this.capacity = capacity;
        }
    }

    /**
     * Calculates the capacity of the given group.
     *
     * @param group
     * @param from                      inclusive
     * @param to                        inclusive
     * @param projectOffIsNotInCapacity if true
     * @return
     */
    public CapacityResult calculateGroupCapacity(@NotNull Group group, @NotNull LocalDate from, @NotNull LocalDate to, boolean projectOffIsNotInCapacity) {
        if (from.isAfter(to)) {
            throw new IllegalArgumentException(String.format("'from' must not be after 'to' ({}, {})", from, to));
        }
        log.debug("Group Capacity for {} from {} to {} ; including projectOff: {}", group.getName(), from, to, projectOffIsNotInCapacity);

        List<Vacation> allVacations = collectAllVacation(group);
        log.debug("Total vacations: {}", allVacations.size());

        List<Vacation> filteredVacations = filterVacations(projectOffIsNotInCapacity, from, to, allVacations);
        int relevantVacations = filteredVacations.size();
        log.debug("Filtered vacations: {}", relevantVacations);

        // Add one, as day counting is exclusive

        List<CalendarDate> calendarDates = calendarDateRepository.findByDateBetween(from, to);
        log.debug("CalendarDates: {}", calendarDates.size());

        Set<LocalDate> bankHolidayDate = bankHolidays(calendarDates);
        log.debug("BankHolidays: {}", bankHolidayDate.size());

        long weekendCount = 0;
        long bankHolidayCount = 0;
        LocalDate currentDate = from;
        do {
            DayOfWeek dayOfWeek = currentDate.getDayOfWeek();
            if (dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY) {
                weekendCount += 1;
            } else if (bankHolidayDate.contains(currentDate)) {
                bankHolidayCount += 1;
            }
            currentDate = currentDate.plusDays(1);
        } while (!currentDate.isAfter(to));
        log.debug("Resulting in {} weekends and {} bankHolidays", weekendCount, bankHolidayCount);

        // +1 is necessary as for between end date is exclusive
        long totalNumberOfDays = ChronoUnit.DAYS.between(from, to) + 1;
        log.debug("Number of weekDays {}", totalNumberOfDays);

        long totalNumberOfWorkingDays = totalNumberOfDays - weekendCount - bankHolidayCount;
        log.debug("Number of working days {}", totalNumberOfWorkingDays);

        long groupSize = group.getMembers().size();
        log.debug("Number of members in group {}: {}", group.getName(), groupSize);

        long manDaysCapacity = groupSize * totalNumberOfWorkingDays;
        log.debug("Pure Capacity in group {}: {}", group.getName(), manDaysCapacity);

        long resultingCapacity = manDaysCapacity - relevantVacations;
        log.debug("Resulting capacity in group {}: {}", group.getName(), resultingCapacity);

        return new CapacityResult(
                group,
                from,
                to,
                projectOffIsNotInCapacity,
                weekendCount,
                bankHolidayCount,
                relevantVacations,
                groupSize,
                totalNumberOfDays,
                resultingCapacity);
    }

    protected Set<LocalDate> bankHolidays(List<CalendarDate> calendarDates) {
        Set<LocalDate> bankHolidays = calendarDates
                .stream()
                .filter(calendarDate -> calendarDate.isBankHoliday())
                .map(calendarDate -> calendarDate.getDate())
                .collect(Collectors.toSet());
        return bankHolidays;
    }

    protected List<Vacation> filterVacations(boolean projectOffIsNotInCapacity, LocalDate from, LocalDate to, List<Vacation> allVacations) {
        return allVacations.stream()
                .filter(vacation -> vacation.getStatus() != VacationStatus.PLANNED)
                .filter(vacation -> vacation.getType() != VacationType.NO_VACATION)
                .filter(vacation ->
                        (vacation.getType() == VacationType.PROJECT_OFF && projectOffIsNotInCapacity)
                                || vacation.getType() != VacationType.PROJECT_OFF)
                .filter(vacation -> (vacation.getDate().isAfter(from) || vacation.getDate().isEqual(from)))
                .filter(vacation -> (vacation.getDate().isBefore(to) || vacation.getDate().isEqual(to)))
                .collect(Collectors.toList());
    }

    private List<Vacation> collectAllVacation(Group group) {
        List<Vacation> allVacations = new ArrayList<>();

        group.getMembers().forEach(person ->
                        allVacations.addAll(person.getVacations())
        );
        return allVacations;
    }
}
