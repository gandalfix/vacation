package de.gandalfix.vacation.domain.services;

import com.google.common.collect.Lists;
import de.gandalfix.vacation.domain.data.entities.Project;
import de.gandalfix.vacation.domain.data.entities.Role;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.repositories.ProjectRepository;
import de.gandalfix.vacation.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    public Project findById(String id) {
        return projectRepository.findOne(id);
    }

    public List<Project> findAll() {
        Iterable<Project> allProjects = projectRepository.findAll();
        List<Project> sorted = Lists.newArrayList(allProjects);
        sorted.sort(Utils.<Project>compare().thenComparing(Project::getName));
        return sorted;
    }

    public List<Project> findProjectsAdministrated(@NotNull User user) {
        List<Project> projects =
                user.getRoles().stream()
                        .filter(Role::isProjectAdmin)
                        .map(Role::getProject)
                        .distinct()
                        .sorted(Utils.<Project>compare().thenComparing(Project::getName))
                        .collect(Collectors.toList());
        return projects;
    }

    /**
     * Checks whether the given user is administrator for the given project
     * @param user Must not be null.
     * @param project Must not be null.
     * @return
     */
    public boolean isAdministrator(@NotNull User user, @NotNull Project project) {
        return findProjectsAdministrated(user).contains(project);
    }
    
    public OperationResult save(@NotNull Project project) {
        projectRepository.save(project);
        return OperationResult.OK();
    }
}
