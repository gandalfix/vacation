package de.gandalfix.vacation.domain.services;

import de.gandalfix.vacation.domain.data.entities.VacationType;

import java.time.LocalDate;

public class OperationResult {
    public final OperationResultOptions status;
    public final String message;

    public OperationResult(OperationResultOptions status, String message) {
        this.status = status;
        this.message = message;
    }

    public boolean isSuccess() {
        return status.success;
    }

    public static OperationResult OK() {
        return new OperationResult(OperationResultOptions.OK, null);
    }

    public static OperationResult FAIL_VACATION_UPDATE(OperationResultOptions result, LocalDate date, VacationType type) {
        return new OperationResult(result, String.format("Vacation for %s could not be updated", date));
    }

    public static OperationResult FAIL_VACATION_DELETE(OperationResultOptions result, LocalDate date) {
        return new OperationResult(result, String.format("Vacation for %s could not be deleted", date));
    }

    public static OperationResult FAIL_STORE_USER(OperationResultOptions result, LocalDate date) {
        return new OperationResult(result, String.format("Vacation for %s could not be deleted", date));
    }
}
