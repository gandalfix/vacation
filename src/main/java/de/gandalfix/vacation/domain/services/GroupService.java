package de.gandalfix.vacation.domain.services;

import de.gandalfix.vacation.domain.data.entities.*;
import de.gandalfix.vacation.domain.repositories.GroupRepository;
import de.gandalfix.vacation.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class GroupService {

    Logger log = LoggerFactory.getLogger(GroupService.class);

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private  ProjectService projectService;

    public List<Person> findPotentialPersonsToAdd(@NotNull User user, @NotNull Group targetGroup) {
        List<Project> administrableProjects = projectService.findProjectsAdministrated(user);
        if (!administrableProjects.contains(targetGroup.getProject())) {
            log.info("Trying to find addable persons for a not allowed project (User: {}, Project {}, Group {})", user.getLogin(), targetGroup.getProject().getName(), targetGroup.getName());
            return new ArrayList<>();
        }
        List<Person> personsToAdd = targetGroup.getProject().getMembers()
                .stream()
                .filter(person -> !targetGroup.getMembers().contains(person))
                .sorted(Utils.<Person>compare().thenComparing(Person::getFullName))
                .collect(Collectors.toList());
        return personsToAdd;
    }

    /**
     * Returns all groups for projects for which the user can see at least for one the team calendar.
     * In case that the user is not able to see anything an empty set is returned.
     * @param supervisor Must not be null.
     * @return
     */
    @Transactional(readOnly = true)
    public Set<Group> findGroupsForTeamCalendar(@NotNull User supervisor) {
        if (!userService.canSeeTeamCalendar(supervisor)) {
            log.warn("User '{}' is trying to see team calendar although not allowed!", supervisor.getLogin());
            return new HashSet<>();
        }

        Set<Project> projects = supervisor.getRoles()
                .stream()
                .filter(Role::isTeamCalendar)
                .map(Role::getProject)
                .collect(Collectors.toSet());

        Optional<Set<Group>> groups =
                projects
                        .stream()
                        .map(Project::getGroups)
                        .reduce((groupies1, groupies2) -> {
                            groupies1.addAll(groupies2);
                            return groupies1;
                        });

        return groups.orElse(new HashSet<>());
    }

    /**
     * Finds a single group by id.
     * @param groupId Must not be null.
     * @return null if no group with that id exists.
     */
    public Group findById(@NotNull String groupId) {
        return groupRepository.findOne(groupId);
    }

    public OperationResult save(Group toSave) {
        groupRepository.save(toSave);
        return OperationResult.OK();
    }

    public OperationResult addPersonToGroup(@NotNull User user, @NotNull Group targetGroup, @NotNull Person personToAdd) {
        List<Person> potentialAddablePersons = findPotentialPersonsToAdd(user, targetGroup);
        if (potentialAddablePersons.contains(personToAdd)) {
            targetGroup.add(personToAdd);
            groupRepository.save(targetGroup);
        }
        return OperationResult.OK();
    }

    public OperationResult removePersonFromGroup(@NotNull User user, @NotNull Group groupToChange, @NotNull Person personToRemove) {
        if (projectService.isAdministrator(user, groupToChange.getProject())) {
            groupToChange.remove(personToRemove);
            groupRepository.save(groupToChange);
        }
        return OperationResult.OK();
    }
}
