package de.gandalfix.vacation.domain.services;

import de.gandalfix.vacation.domain.data.entities.Person;
import de.gandalfix.vacation.domain.data.entities.Role;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public OperationResult save(Person personToSave) {
        personRepository.save(personToSave);
        return OperationResult.OK();
    }

    /**
     * Returns all persons that are in projects where the given user has granting permission
     * @param supervisor
     * @return not null set of persons
     */
    @Transactional(readOnly = true)
    public Set<Person> getSubordinates(@NotNull User supervisor) {
        Set<Role> grantingRoles = supervisor.getRoles()
                .stream()
                .filter(role -> role.isGrant())
                .collect(Collectors.toSet());

        Set<Person> subordinates = new HashSet<>();
        grantingRoles.forEach(role -> {
            subordinates.addAll(role.getProject().getMembers());
        });

        return subordinates;
    };

    public List<Person> findPerson(String searchString) {
        return personRepository.findPersonBySearchString(searchString);
    }

    public Person findById(String personId) {
        return personRepository.findOne(personId);
    }

    public List<Person> findWithoutUsers() {
        return personRepository.findPersonsWithoutUser();
    }
}
