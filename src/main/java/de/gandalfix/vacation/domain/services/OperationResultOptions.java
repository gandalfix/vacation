package de.gandalfix.vacation.domain.services;

public enum OperationResultOptions {
    OK(true),
    NOT_IN_PLANNING_ANYMORE(false),
    DATABASE_OPERATION_FAILED(false),
    NOT_ENOUGH_SPARE_VACATION(false);

    public final boolean success;

    OperationResultOptions(boolean success) {
        this.success = success;
    }
}
