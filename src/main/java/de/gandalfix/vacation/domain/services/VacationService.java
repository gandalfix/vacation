package de.gandalfix.vacation.domain.services;

import de.gandalfix.vacation.domain.data.entities.*;
import de.gandalfix.vacation.domain.repositories.VacationRepository;
import de.gandalfix.vacation.util.VacationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class VacationService {

    @Autowired
    private VacationRepository vacationRepository;

    @Autowired
    private PersonService personService;

    /**
     * Updates a vacation entry specified by the parameters, person and vacationDate.
     * If the entry already exists it is updated, otherwise a new one is created.
     * If type is null, the existing entry is deleted.
     * @param person person the vacation is for. Must not be null.
     * @param vacationDate date of the vacation. Must not be null.
     * @param type type of the vacation. If null the vacation is deleted.
     * @param status new status of the vacation. Must not be null.
     * @return
     */
    public OperationResult updateVacation(Person person, LocalDate vacationDate, VacationType type, VacationStatus status) {
        if (type != null && type != VacationType.NO_VACATION) {
            return persistVacation(person, vacationDate, type);
        }
        return deleteVacation(person, vacationDate);
    }

    /**
     * Allows the given user to grant vacation.
     * If the user is not supervisor for a person the vacation will be ignored and nothing happens.
     * @param supervisor User that grants the vacation. Must be not null.
     * @param vacationToGrant Map of person to vacation date
     * @return
     */
    public OperationResult grantVacation(@NotNull User supervisor, @NotNull Map<Person, Set<LocalDate>> vacationToGrant) {
        // TODO write approval mail
        return changeVacationStatus(supervisor, vacationToGrant, VacationStatus.GRANTED);
    }

    public OperationResult rejectVacation(@NotNull User supervisor, @NotNull Map<Person, Set<LocalDate>> vacationToGrant) {
        // TODO Write rejection mail
        return changeVacationStatus(supervisor, vacationToGrant, VacationStatus.PLANNED);
    }

    private OperationResult changeVacationStatus(User supervisor, Map<Person, Set<LocalDate>> vacationToGrant, VacationStatus targetStatus) {
        Set<Person> subordinates = personService.getSubordinates(supervisor);

        for (Person person: vacationToGrant.keySet()) {
            if (subordinates.contains(person)) {
                Set<LocalDate> datesToGrant = vacationToGrant.get(person);
                person.getVacations()
                        .stream()
                        .filter(vacation -> vacation.getStatus() == VacationStatus.SUBMITTED) // Only already submitted vacations are allowed
                        .filter(vacation -> datesToGrant.contains(vacation.getDate())) // Only if we should grant them
                        .forEach( // And in the end just save it
                                vacation -> {
                                    vacation.setStatus(targetStatus);
                                    vacationRepository.save(vacation);
                                }
                        );

            }
        }

        return OperationResult.OK();
    }

    /**
     * Submits for the given person a list of vacation dates.
     * For this a vacation entry in "planning" has to exist for each entry.
     * If not the date is just ignored.
     *
     * @param person Must not be null.
     * @param vacationDates List of dates for which a vacation should be submitted. Must not be null.
     * @return
     */
    public OperationResult submitVacation(Person person, List<LocalDate> vacationDates) {
        Set<LocalDate> vacationDatesToSubmit = new HashSet<>(vacationDates);

        List<Vacation> currentVacations = findAll(person);

        List<Vacation> vacationsToNewlySubmit = currentVacations.stream()
                .filter(vacation -> vacation.getStatus() == VacationStatus.PLANNED)
                .filter(vacation -> vacationDatesToSubmit.contains(vacation.getDate()))
                .collect(Collectors.toList());

        vacationsToNewlySubmit.forEach(vacation -> {
            vacation.setStatus(VacationStatus.SUBMITTED);
            vacationRepository.save(vacation);
        });

        return OperationResult.OK();
    }

    public List<Vacation> findAll(Person person) {
        return vacationRepository.findByPerson(person);
    }

    private OperationResult deleteVacation(Person person, LocalDate vacationDate) {
        List<Vacation> vacations = vacationRepository.findByPersonAndDate(person, vacationDate);
        if (!vacations.isEmpty()) {
            Vacation existingVacation = vacations.get(0);
            if (existingVacation.getStatus() == VacationStatus.PLANNED) {
                person.remove(existingVacation);
                vacationRepository.delete(existingVacation);
            } else {
                return OperationResult.FAIL_VACATION_DELETE(OperationResultOptions.NOT_IN_PLANNING_ANYMORE, vacationDate);
            }
        }
        return OperationResult.OK();
    }

    private OperationResult persistVacation(Person person, LocalDate vacationDate, VacationType type) {
        List<Vacation> vacations = vacationRepository.findByPersonAndDate(person, vacationDate);
        Vacation vacation = null;
        if (vacations.isEmpty()) {
            if (!VacationUtil.isEnoughVacationLeft(person, vacationDate, type, null)) {
                return new OperationResult(OperationResultOptions.NOT_ENOUGH_SPARE_VACATION, "Not enough vacation left");
            }
            vacation = new Vacation(person, vacationDate, VacationStatus.PLANNED, type);
        } else {
            // TODO currently here is a small whole in the logic that allows the person to get more vacation than actually allowed
            // It is possible to first create a lot of half-day vacations (eg. 60 of them if you have 30 days vacation)
            // after that switch all of them to full days.
            // But still someone has to approve them ;-)
            vacation = vacations.get(0);
            if (vacation.getStatus() == VacationStatus.PLANNED) {
                vacation.setType(type);
            } else {
                return OperationResult.FAIL_VACATION_UPDATE(OperationResultOptions.NOT_IN_PLANNING_ANYMORE, vacationDate, type);
            }
        }
        person.add(vacation);
        vacationRepository.save(vacation);
        return OperationResult.OK();
    }
}
