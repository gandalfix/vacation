package de.gandalfix.vacation.domain.services;

import de.gandalfix.vacation.domain.data.entities.Role;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.function.Predicate;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User findById(String id) {
        return userRepository.findOne(id);
    }

    /**
     * Returns whether the user has at least one role in which he is allowed to grant vacation.
     */
    public boolean isSupervisor(@NotNull User user) {
        return hasRole(user, Role::isGrant);
    }

    /**
     * Returns whether the user has at least one role in which he is project admin.
     */
    public boolean isAdmin(@NotNull User user) {
        return hasRole(user, Role::isProjectAdmin);
    }

    /**
     * Returns whether the user can at least see one team calendar
     */
    public boolean canSeeTeamCalendar(@NotNull User user) {
        return hasRole(user, Role::isTeamCalendar);
    }

    private boolean hasRole(User user, Predicate<Role> filter) {
        long filteredCount = user.getRoles()
                .stream()
                .filter(filter)
                .count();
        return filteredCount > 0;
    }

    public OperationResult save(@NotNull User toStore) {
        try {
            userRepository.save(toStore);
        } catch (RuntimeException e) {
            return OperationResult.FAIL_STORE_USER(OperationResultOptions.DATABASE_OPERATION_FAILED, LocalDate.now());
        }
        return OperationResult.OK();
    }
}
