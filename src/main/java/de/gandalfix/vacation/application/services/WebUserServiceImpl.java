package de.gandalfix.vacation.application.services;

import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.repositories.UserRepository;
import de.gandalfix.vacation.domain.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service to handle user authentication.
 * Esp. converting application specific users to spring security users;
 */
@Service("userDetailService")
public class WebUserServiceImpl implements UserDetailsService, WebUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(username);
        if (user == null) {
            return throwUnknownUserError();
        }

        List<GrantedAuthority> authorities = buildUserAuthority(user);
        return buildUserForAuthentication(user, authorities);
    }

    private org.springframework.security.core.userdetails.User buildUserForAuthentication(User user,
                                                                                          List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(
                user.getLogin(),
                user.getPassword(),
                user.isEnabled(),
                /*accountNonExpired*/true,
                /*credentialsNonExpired*/true,
                /*accountNonLocked*/true,
                authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(User user) {

        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        setAuths.add(new SimpleGrantedAuthority("ROLE_USER"));

        if (userService.isSupervisor(user)) {
            setAuths.add(new SimpleGrantedAuthority("ROLE_SUPERVISOR"));
        }

        if (userService.canSeeTeamCalendar(user)) {
            setAuths.add(new SimpleGrantedAuthority("ROLE_TEAM_CALENDAR"));
        }

        if (userService.isAdmin(user)) {
            setAuths.add(new SimpleGrantedAuthority("ROLE_PROJECT_ADMIN"));
        }
        if (user.isUserAdmin()) {
            setAuths.add(new SimpleGrantedAuthority("ROLE_USER_ADMIN"));
        }

        List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(setAuths);
        return result;
    }

    private UserDetails throwUnknownUserError() {
        throw new UsernameNotFoundException("Username or password incorrect");
    }
}
