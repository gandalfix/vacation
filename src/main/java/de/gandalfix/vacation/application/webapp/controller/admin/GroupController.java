package de.gandalfix.vacation.application.webapp.controller.admin;

import com.google.common.base.Strings;
import de.gandalfix.vacation.application.webapp.data.exception.ElementNotFoundException;
import de.gandalfix.vacation.domain.data.entities.Group;
import de.gandalfix.vacation.domain.data.entities.Person;
import de.gandalfix.vacation.domain.services.GroupService;
import de.gandalfix.vacation.domain.services.PersonService;
import de.gandalfix.vacation.domain.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

// TODO add some more additional security
@Controller
public class GroupController extends AbstractAdminController<Group> {

    @Autowired
    private GroupService groupService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private PersonService personService;

    @RequestMapping("/admin/group")
    public String show(@RequestParam(value = "id", required = false) String id, Model model) {
        fillLoggedInModelParameter(model);
        Group toEdit;
        if (Strings.isNullOrEmpty(id)) {
            toEdit = new Group();
        } else {
            toEdit = findById(id);
            if (toEdit == null) {
                throw new ElementNotFoundException("No element with id " + id);
            }
            model.addAttribute("personsToAdd", groupService.findPotentialPersonsToAdd(getUser(), toEdit));
        }
        model.addAttribute("toEdit", toEdit);
        model.addAttribute("projects", projectService.findProjectsAdministrated(getUser()));
        return "admin/group";
    }

    @RequestMapping(value = "/admin/group", method = RequestMethod.POST)
    public String edit(@ModelAttribute Group toEdit, @RequestParam("action") String action, Model model) {
        fillLoggedInModelParameter(model);

        Group toStore;
        String id = toEdit.getId();
        if (Strings.isNullOrEmpty(id)) {
            // If there is no id, normally it is sent as empty string
            toEdit.setId(null);
            toStore = toEdit;
        } else {
            toStore = findById(id);
            if (toStore == null) {
                throw new ElementNotFoundException("No element with id " + id);
            }
            toStore.setName(toEdit.getName());
        }
        groupService.save(toStore);
        return String.format("redirect:/admin/group?id=%s", toStore.getId());
    }

    @RequestMapping(value = "/admin/groupAddPerson", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute Group toEdit, @RequestParam("personToAdd") String personToAddId, Model model) {
        fillLoggedInModelParameter(model);

        Group toStore;
        String id = toEdit.getId();
        if (Strings.isNullOrEmpty(id)) {
            throw new ElementNotFoundException("Only able to add persons to already saved groups");
        }
        toStore = findById(id);
        if (toStore == null) {
            throw new ElementNotFoundException("No group with id " + id);
        }

        Person personToAdd = personService.findById(personToAddId);
        if (personToAdd == null) {
            throw new ElementNotFoundException("No person with id " + personToAddId);
        }

        groupService.addPersonToGroup(getUser(), toStore, personToAdd);
        return String.format("redirect:/admin/group?id=%s", toStore.getId());
    }

    @RequestMapping(value = "/admin/groupDeletePerson", method = RequestMethod.GET)
    public String deletePerson(@RequestParam("personId") String personId, @RequestParam("groupId") String groupId) {

        Group groupToChange = findById(groupId);
        if (groupToChange == null) {
            throw new ElementNotFoundException("No group with id " + groupId);
        }
        if (!projectService.isAdministrator(getUser(), groupToChange.getProject())) {
            throw new ElementNotFoundException("No group with id " + groupId);
        }
        Person personToRemove = personService.findById(personId);
        if (personToRemove == null) {
            throw new ElementNotFoundException("No person with id " + personId);
        }

        groupService.removePersonFromGroup(getUser(), groupToChange, personToRemove);
        return String.format("redirect:/admin/group?id=%s", groupId);
    }

    @Override
    protected Group findById(String id) {
        return groupService.findById(id);
    }
}
