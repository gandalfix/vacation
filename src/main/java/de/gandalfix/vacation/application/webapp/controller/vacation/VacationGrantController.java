package de.gandalfix.vacation.application.webapp.controller.vacation;

import de.gandalfix.vacation.application.webapp.controller.AbstractController;
import de.gandalfix.vacation.application.webapp.data.GrantResponseData;
import de.gandalfix.vacation.application.webapp.data.PersonData;
import de.gandalfix.vacation.application.webapp.data.VacationData;
import de.gandalfix.vacation.domain.data.entities.Person;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.data.entities.VacationStatus;
import de.gandalfix.vacation.domain.services.OperationResult;
import de.gandalfix.vacation.domain.services.PersonService;
import de.gandalfix.vacation.domain.services.VacationService;
import de.gandalfix.vacation.util.DateUtil;
import de.gandalfix.vacation.util.VacationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class VacationGrantController extends AbstractController {

    Logger log = LoggerFactory.getLogger(VacationGrantController.class);

    @Autowired
    private PersonService personService;

    @Autowired
    private VacationService vacationService;

    @RequestMapping("/vacationgrant")
    public String vacationSubmit(Model model) {
        fillLoggedInModelParameter(model);
        return "vacationgrant";
    }

    /**
     * input: "day_YYYY-MM-DD_personId"
     *
     * @param grantRequests
     * @param model
     * @return
     */
    @RequestMapping(value = "/vacationgrant", method = RequestMethod.POST)
    public String vacationSubmit(@RequestParam(value = "GRANT_VACATION", required = false) List<String> grantRequests, @RequestParam(value = "action", required = true) String action, Model model) {
        fillLoggedInModelParameter(model);

        if (grantRequests == null || grantRequests.isEmpty()) {
            log.debug("Did not receive any days for submitting");
            return "redirect:/vacationgrant";
        }

        List<String[]> splitted = splitDateStringsIntoArrays(grantRequests);

        Map<Person, Set<LocalDate>> grantMap = new HashMap<>();
        Map<String, Person> knownPersonIds = new HashMap<>();

        splitted.forEach(
                stringArray -> {
                    String personId = stringArray[2];
                    String stringDate = stringArray[1];
                    LocalDate date = DateUtil.parsePostDate(stringDate);
                    Person person = knownPersonIds.get(personId);
                    if (person == null) {
                        person = personService.findById(personId);
                        knownPersonIds.put(personId, person);
                    }

                    if (!grantMap.containsKey(person)) {
                        grantMap.put(person, new HashSet<>());
                    }
                    grantMap.get(person).add(date);
                }
        );

        if (action != null) {
            if ("GRANT".equals(action.toUpperCase())) {
                vacationService.grantVacation(getUser(), grantMap);
            } else if ("REJECT".equals(action.toUpperCase())) {
                vacationService.rejectVacation(getUser(), grantMap);
            }
        }

        return "redirect:/vacationgrant";
    }

    private List<String[]> splitDateStringsIntoArrays(List<String> grantRequests) {
        return grantRequests
                .stream()
                .filter(grantRequest -> grantRequest.startsWith(VacationSubmitController.DATE_PREFIX))
                .map(grantRequest -> grantRequest.split("_"))
                .filter(stringArray -> stringArray.length == 3) // only want to have entries that are what we expect
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/vacationgrantdetails", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GrantResponseData> vacationGrant(@RequestParam(value = "group", required = false) String group) {
        User user = getUser();
        Set<Person> persons = personService.getSubordinates(user);

        // Create the UI objects for vacation
        Map<Person, Set<VacationData>> personDataToVacation = persons.stream()
                .collect(Collectors.toMap(
                        person -> person,
                        person -> VacationUtil.vacationsToWebData(person.getVacations())
                ));

        // After that do the mapping of vacation to person-id and person data to person-id
        // So the GUI can do the correlation
        Map<String, Set<VacationData>> personIdToVacation = new HashMap<>();
        Map<String, PersonData> personData = new HashMap<>();
        for (Person p : persons) {
            Set<VacationData> filteredVacationData = personDataToVacation.get(p)
                    .stream()
                    .filter(vacationData -> vacationData.status == VacationStatus.SUBMITTED)
                    .collect(Collectors.toSet());
            if (!filteredVacationData.isEmpty()) {
                personIdToVacation.put(p.getId(), filteredVacationData);
                personData.put(p.getId(), new PersonData(p));
            }
        }
        return new ResponseEntity<GrantResponseData>(
                new GrantResponseData(
                        OperationResult.OK(),
                        getUserPersonData(user),
                        personIdToVacation,
                        personData),
                HttpStatus.OK);
    }
}
