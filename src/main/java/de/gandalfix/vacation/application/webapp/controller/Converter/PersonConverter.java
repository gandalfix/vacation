package de.gandalfix.vacation.application.webapp.controller.Converter;

import de.gandalfix.vacation.domain.data.entities.Person;
import de.gandalfix.vacation.domain.services.PersonService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PersonConverter implements Converter<String, Person> {

    @Autowired
    private BeanFactory beanFactory;

    @Override
    public Person convert(String source) {
        PersonService personService = beanFactory.getBean(PersonService.class);
        return personService.findById(source);
    }
}
