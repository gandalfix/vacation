package de.gandalfix.vacation.application.webapp.data;

import de.gandalfix.vacation.domain.services.OperationResult;

import java.util.Collection;
import java.util.List;

public class ResponseData {
    public final PersonData person;
    public final Collection<VacationData> vacations;
    public final OperationResult operationResult;
    public final List<VacationPerYearData> vacationPerYear;

    public ResponseData(OperationResult operationResult, PersonData person, Collection<VacationData> vacations, List<VacationPerYearData> vacationPerYear) {
        this.person = person;
        this.vacations = vacations;
        this.operationResult = operationResult;
        this.vacationPerYear = vacationPerYear;
    }

    public ResponseData(PersonData person, Collection<VacationData> vacations, List<VacationPerYearData> vacationPerYear) {
        this(null, person, vacations, vacationPerYear);
    }
}
