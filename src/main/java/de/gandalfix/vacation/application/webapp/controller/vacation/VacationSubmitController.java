package de.gandalfix.vacation.application.webapp.controller.vacation;

import de.gandalfix.vacation.application.webapp.controller.AbstractController;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.services.VacationService;
import de.gandalfix.vacation.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class VacationSubmitController extends AbstractController {

    Logger log = LoggerFactory.getLogger(VacationSubmitController.class);

    public static final String DATE_PREFIX = "day_";

    @Autowired
    private VacationService vacationService;

    @RequestMapping("/vacationsubmit")
    public String vacationSubmit(Model model) {
        fillLoggedInModelParameter(model);
        return "vacationsubmit";
    }

    @RequestMapping(value = "/vacationsubmit", method = RequestMethod.POST)
    public String vacationSubmit(@RequestParam(value = "SUBMIT_VACATION", required = false) List<String> days, Model model) {
        fillLoggedInModelParameter(model);

        if (days == null || days.isEmpty()) {
            log.debug("Did not receive any days for submitting");
            return "redirect:/vacationsubmit";
        }

        List<LocalDate> dates = days.stream()
                .filter(day -> day.startsWith(DATE_PREFIX))
                .map(day -> day.substring(DATE_PREFIX.length()))
                .map((day -> DateUtil.parsePostDate(day)))
                .collect(Collectors.toList());

        User user = getUser();
        vacationService.submitVacation(user.getPerson(), dates);

        return "redirect:/vacationsubmit";
    }
}
