package de.gandalfix.vacation.application.webapp.data;

import de.gandalfix.vacation.domain.data.entities.VacationPerYear;

public class VacationPerYearData {
    public final int year;
    public final int vacationHours;
    public final int vacationHoursLeft;

    public VacationPerYearData(VacationPerYear vacationPerYear, int vacationHoursLeft) {
        this.year = vacationPerYear.getYear();
        this.vacationHours = vacationPerYear.getVacationHours();
        this.vacationHoursLeft = vacationHoursLeft;
    }
}
