package de.gandalfix.vacation.application.webapp.controller.vacation;

import de.gandalfix.vacation.application.webapp.controller.AbstractController;
import de.gandalfix.vacation.domain.data.entities.CalendarDate;
import de.gandalfix.vacation.domain.repositories.CalendarDateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
public class CalendarController extends AbstractController {

    @Autowired
    private CalendarDateRepository calendarDateRepository;

    @RequestMapping(value = "/calendar", method= RequestMethod.GET)
    public List<CalendarDate> calendarDates(@RequestParam(value="year", defaultValue="0") int year) {
        if (year == 0) {
            year = LocalDate.now().getYear();
        }
        LocalDate startOf = LocalDate.of(year, 1, 1);
        LocalDate endOf = LocalDate.of(year, 12, 31);

        List<CalendarDate> byDateBetween = calendarDateRepository.findByDateBetween(startOf, endOf);
        return byDateBetween;
    }
}
