package de.gandalfix.vacation.application.webapp.data;

import de.gandalfix.vacation.domain.data.entities.Vacation;
import de.gandalfix.vacation.domain.data.entities.VacationStatus;
import de.gandalfix.vacation.domain.data.entities.VacationType;

import java.time.LocalDate;

/**
 * Frontend wrapper to avoid serializing the whole object graph.
 */
public class VacationData {

    public final LocalDate date;
    public final VacationStatus status;
    public final VacationType type;

    public VacationData(Vacation vacation) {
        date = vacation.getDate();
        status = vacation.getStatus();
        type = vacation.getType();
    }
}
