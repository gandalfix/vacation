package de.gandalfix.vacation.application.webapp.data;

import de.gandalfix.vacation.domain.services.OperationResult;

import java.util.Map;
import java.util.Set;

/**
 * Transport class for the "grant" pages
 */
public class GrantResponseData {

    public final PersonData person;
    public final Map<String, Set<VacationData>> vacations;
    public final Map<String, PersonData> subordinates;
    public final OperationResult operationResult;

    public GrantResponseData(OperationResult operationResult, PersonData person, Map<String, Set<VacationData>> vacations, Map<String, PersonData> subordinates) {
        this.person = person;
        this.vacations = vacations;
        this.subordinates = subordinates;
        this.operationResult = operationResult;
    }
}
