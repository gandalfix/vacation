package de.gandalfix.vacation.application.webapp.controller.vacation;

import de.gandalfix.vacation.application.webapp.controller.AbstractController;
import de.gandalfix.vacation.application.webapp.data.GroupData;
import de.gandalfix.vacation.application.webapp.data.PersonData;
import de.gandalfix.vacation.application.webapp.data.TeamCalendarResponseData;
import de.gandalfix.vacation.application.webapp.data.VacationData;
import de.gandalfix.vacation.domain.data.entities.Group;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.services.CapacityService;
import de.gandalfix.vacation.domain.services.GroupService;
import de.gandalfix.vacation.domain.services.OperationResult;
import de.gandalfix.vacation.domain.services.UserService;
import de.gandalfix.vacation.util.VacationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.*;

@Controller
public class TeamCalendarController extends AbstractController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserService userService;

    @Autowired
    private CapacityService capacityService;

    @RequestMapping(value = "/teamcalendar", method = RequestMethod.GET)
    public String vacationOverview(Model model) {
        User user = getUser();
        fillLoggedInModelParameter(model);
        List<Group> groups = new ArrayList<>(groupService.findGroupsForTeamCalendar(user));
        groups.sort((g1, g2) -> {
            int projectCompare = g1.getProject().getName().compareTo(g2.getProject().getName());
            if (projectCompare == 0) {
                return g1.getName().compareTo(g2.getName());
            }
            return projectCompare;
        });
        model.addAttribute("groups", groups);
        return "teamcalendar";
    }

    @RequestMapping(value = "/teamcalendar", method = RequestMethod.POST)
    public ResponseEntity<TeamCalendarResponseData> vacationOverview(@RequestParam(value = "groupId") String groupId,
                                                                     @RequestParam("yearToDisplay") int yearToDisplay) {
        User user = getUser();
        if (!userService.canSeeTeamCalendar(user)) {
            return new ResponseEntity<TeamCalendarResponseData>(HttpStatus.FORBIDDEN);
        }

        Group group = groupService.findById(groupId);
        if (group == null) {
            return new ResponseEntity<TeamCalendarResponseData>(HttpStatus.BAD_REQUEST);
        }

        //==================
        LocalDate start = LocalDate.of(2014, 1, 6);
        LocalDate end = LocalDate.of(2014, 1, 12);
        capacityService.calculateGroupCapacity(group, start, end, true);
        //==================

        GroupData groupData = new GroupData(group);
        Map<String, PersonData> idToPerson = new HashMap<>();
        Map<String, Set<VacationData>> personToVacation = new HashMap<>();
        group.getMembers().forEach(person -> idToPerson.put(person.getId(), new PersonData(person)));
        group.getMembers().forEach(person -> {
            Set<VacationData> vacations = VacationUtil.vacationsToWebData(person.getVacations());
            if (!vacations.isEmpty()) {
                personToVacation.put(person.getId(), vacations);
            }
        });

        return new ResponseEntity<TeamCalendarResponseData>(
                new TeamCalendarResponseData(
                        OperationResult.OK(),
                        getUserPersonData(user),
                        groupData,
                        idToPerson,
                        personToVacation
                ),
                HttpStatus.ACCEPTED);
    }
}
