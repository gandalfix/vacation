package de.gandalfix.vacation.application.webapp.controller.admin;

import com.google.common.base.Strings;
import de.gandalfix.vacation.application.webapp.data.PersonData;
import de.gandalfix.vacation.application.webapp.data.PersonResponseData;
import de.gandalfix.vacation.application.webapp.data.exception.ElementNotFoundException;
import de.gandalfix.vacation.domain.data.entities.Person;
import de.gandalfix.vacation.domain.services.PersonService;
import de.gandalfix.vacation.util.PersonUtil;
import de.gandalfix.vacation.util.Utils;
import org.hibernate.loader.custom.Return;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// TODO add some more additional security
@Controller
public class PersonController extends AbstractAdminController<Person> {

    @Autowired
    private PersonService personService;

    @RequestMapping("/admin/personsearch")
    public String startSearch(Model model) {
        fillLoggedInModelParameter(model);
        return "admin/personsearch";
    }

    @RequestMapping(value = "/admin/personsearch", method = RequestMethod.POST)
    public String search(@RequestParam String searchString, Model model) {
        fillLoggedInModelParameter(model);
        List<Person> persons = personService.findPerson(searchString);
        model.addAttribute("searchResult", persons);
        return "admin/personsearch";
    }

    @RequestMapping("/admin/person")
    public String show(@RequestParam(value = "id", required = false) String id, Model model) {
        fillLoggedInModelParameter(model);
        Person personToEdit;
        if (Strings.isNullOrEmpty(id)) {
            personToEdit = new Person();
        } else {
            personToEdit = findById(id);
            if (personToEdit == null) {
                throw new ElementNotFoundException("No element with id " + id);
            }
        }
        model.addAttribute("toEdit", personToEdit);
        model.addAttribute("titles", possibleTitles());
        return "admin/person";
    }

    @RequestMapping(value = "/admin/person", method = RequestMethod.POST)
    public String edit(@ModelAttribute Person personToEdit, Model model) {
        fillLoggedInModelParameter(model);

        Person personToStore;
        String personId = personToEdit.getId();
        if (Strings.isNullOrEmpty(personId)) {
            // If there is no id, normally it is sent as empty string
            personToEdit.setId(null);
            personToStore = personToEdit;
        } else {
            personToStore = findById(personId);
            if (personToStore == null) {
                throw new ElementNotFoundException("No element with id " + personId);
            }
            PersonUtil.fillFromPerson(personToStore, personToEdit);
        }

        personService.save(personToStore);

        return String.format("redirect:/admin/person?id=%s", personToStore.getId());
    }

    private List<String> possibleTitles() {
        List<String> titles = new ArrayList<>();
        titles.add("");
        titles.add("Dr.");
        titles.add("Prof.");
        return titles;
    }

    @Override
    protected Person findById(String id) {
        return personService.findById(id);
    }
}
