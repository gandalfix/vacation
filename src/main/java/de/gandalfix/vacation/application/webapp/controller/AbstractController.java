package de.gandalfix.vacation.application.webapp.controller;

import de.gandalfix.vacation.application.webapp.data.PersonData;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.repositories.UserRepository;
import de.gandalfix.vacation.domain.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;

public abstract class AbstractController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    protected UserService userService;

    protected User getUser() {
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findByLogin(user.getUsername());
    }

    protected PersonData getUserPersonData(User user) {
        return new PersonData(user.getPerson());
    }

    protected void fillLoggedInModelParameter(Model model) {
        User user = getUser();
        model.addAttribute("user", user);
        model.addAttribute("person", user.getPerson());
        model.addAttribute("isSupervisor", userService.isSupervisor(user));
        model.addAttribute("isProjectAdmin", userService.isAdmin(user));
    }
}
