package de.gandalfix.vacation.application.webapp.controller.vacation;

import com.google.common.base.Strings;
import de.gandalfix.vacation.application.webapp.controller.AbstractController;
import de.gandalfix.vacation.application.webapp.data.ResponseData;
import de.gandalfix.vacation.application.webapp.data.VacationData;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.data.entities.VacationStatus;
import de.gandalfix.vacation.domain.data.entities.VacationType;
import de.gandalfix.vacation.domain.repositories.PersonRepository;
import de.gandalfix.vacation.domain.services.OperationResult;
import de.gandalfix.vacation.domain.services.VacationService;
import de.gandalfix.vacation.util.DateUtil;
import de.gandalfix.vacation.util.VacationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Controller
public class VacationController extends AbstractController {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private VacationService vacationService;

    @RequestMapping("/vacation")
    public String vacationOverview(Model model) {
        fillLoggedInModelParameter(model);
        return "vacation";
    }

    @RequestMapping("/vacationDetail")
    public
    @ResponseBody
    ResponseData vacationDetail() {
        User user = getUser();
        return getResponseDataFromUser(user);
    }

    @RequestMapping(value = "/vacationUpdate", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<ResponseData> vacationUpdate(@NotNull String date, @NotNull String type) {
        User user = getUser();

        if (Strings.isNullOrEmpty(date)) {
            return new ResponseEntity<ResponseData>(getResponseDataFromUser(user), HttpStatus.BAD_REQUEST);
        }

        LocalDate vacationDate = DateUtil.parsePostDate(date);

        if ("&nbsp;".equals(type)) {
            type = "";
        }

        OperationResult result = vacationService.updateVacation(user.getPerson(), vacationDate, VacationType.fromAbbreviation(type), VacationStatus.PLANNED);

        Set<VacationData> vacationData = VacationUtil.vacationsToWebData(vacationService.findAll(user.getPerson()));

        return new ResponseEntity<ResponseData>(
                new ResponseData(
                        result,
                        getUserPersonData(user),
                        vacationData,
                        VacationUtil.vacationAmountToWebData(user.getPerson(), user.getPerson().getVacationPerYear())),
                result.status.success ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    private ResponseData getResponseDataFromUser(User user) {
        Set<VacationData> vacationData = VacationUtil.vacationsToWebData(user.getPerson().getVacations());
        ResponseData response = new ResponseData(
                getUserPersonData(user),
                vacationData,
                VacationUtil.vacationAmountToWebData(user.getPerson(), user.getPerson().getVacationPerYear()));
        return response;
    }
}
