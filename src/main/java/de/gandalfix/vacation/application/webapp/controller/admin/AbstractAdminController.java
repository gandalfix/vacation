package de.gandalfix.vacation.application.webapp.controller.admin;

import de.gandalfix.vacation.application.webapp.controller.AbstractController;
import de.gandalfix.vacation.domain.data.entities.BaseEntity;

// TODO preparation for cleaning up the admin controllers as they all look the same
public abstract class AbstractAdminController<T extends BaseEntity> extends AbstractController {

    protected abstract T findById(String id);
}
