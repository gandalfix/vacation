package de.gandalfix.vacation.application.webapp.data;

import de.gandalfix.vacation.domain.data.entities.Group;
import de.gandalfix.vacation.domain.data.entities.Person;

import java.util.HashSet;
import java.util.Set;

public class GroupData {

    public final String id;
    public final String name;
    public final String projectName;
    public final Set<PersonData> members = new HashSet<>();

    public GroupData(Group group) {
        id = group.getId();
        name = group.getName();
        projectName = group.getProject().getName();
        for (Person member: group.getMembers()) {
            members.add(new PersonData(member));
        }
    }
}
