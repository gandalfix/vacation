package de.gandalfix.vacation.application.webapp.data;

import de.gandalfix.vacation.domain.data.entities.Person;

/**
 * Frontend wrapper to avoid serializing the whole object graph.
 */
public class PersonData {

    public final String id;
    public final String title;
    public final String firstName;
    public final String lastName;
    public final String code;
    public final String fullName;

    public PersonData(Person person) {
        id = person.getId();
        title = person.getTitle();
        firstName = person.getFirstName();
        lastName = person.getLastName();
        code = person.getCode();
        fullName = person.getFullName();
    }
}
