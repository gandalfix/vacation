package de.gandalfix.vacation.application.webapp.controller.admin;

import com.google.common.base.Strings;
import de.gandalfix.vacation.application.webapp.data.exception.ElementNotFoundException;
import de.gandalfix.vacation.domain.data.entities.Project;
import de.gandalfix.vacation.domain.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

// TODO add some more additional security
@Controller
public class ProjectController extends AbstractAdminController<Project> {

    @Autowired
    private ProjectService projectService;

    @RequestMapping("/admin/project")
    public String show(@RequestParam(value = "id", required = false) String id, Model model) {
        fillLoggedInModelParameter(model);
        Project toEdit;
        if (Strings.isNullOrEmpty(id)) {
            toEdit = new Project();
        } else {
            toEdit = findById(id);
            if (toEdit == null) {
                throw new ElementNotFoundException("No element with id " + id);
            }
        }
        model.addAttribute("toEdit", toEdit);
        return "admin/project";
    }

    @RequestMapping(value = "/admin/project", method = RequestMethod.POST)
    public String edit(@ModelAttribute Project toEdit, Model model) {
        fillLoggedInModelParameter(model);

        Project toStore;
        String id = toEdit.getId();
        if (Strings.isNullOrEmpty(id)) {
            // If there is no id, normally it is sent as empty string
            toEdit.setId(null);
            toStore = toEdit;
        } else {
            toStore = findById(id);
            if (toStore == null) {
                throw new ElementNotFoundException("No element with id " + id);
            }
            toStore.setName(toEdit.getName());
        }

        projectService.save(toStore);

        return String.format("redirect:/admin/project?id=%s", toStore.getId());
    }

    @RequestMapping("/admin/projectoverview")
    public String showOverview(Model model) {
        fillLoggedInModelParameter(model);
        model.addAttribute("projects", projectService.findProjectsAdministrated(getUser()));
        return "admin/projectoverview";
    }

    @Override
    protected Project findById(String id) {
        return projectService.findById(id);
    }
}
