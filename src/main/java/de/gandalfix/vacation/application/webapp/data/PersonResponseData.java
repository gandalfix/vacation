package de.gandalfix.vacation.application.webapp.data;

import de.gandalfix.vacation.domain.data.entities.Person;

import java.util.List;

public class PersonResponseData {
    public final List<PersonData> persons;

    public PersonResponseData(List<PersonData> persons) {
        this.persons = persons;
    }

}
