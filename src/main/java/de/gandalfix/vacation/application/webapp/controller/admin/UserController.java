package de.gandalfix.vacation.application.webapp.controller.admin;

import com.google.common.base.Strings;
import de.gandalfix.vacation.application.webapp.config.WebSecurityConfig;
import de.gandalfix.vacation.application.webapp.data.exception.ElementNotFoundException;
import de.gandalfix.vacation.application.webapp.data.exception.UnknownActionException;
import de.gandalfix.vacation.domain.data.entities.Person;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.services.OperationResult;
import de.gandalfix.vacation.domain.services.PersonService;
import de.gandalfix.vacation.domain.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Controller
public class UserController extends AbstractAdminController<User> {

    @Autowired
    private UserService userService;

    @Autowired
    private PersonService personService;

    @Autowired
    private WebSecurityConfig webSecurityConfig;

    public static class PasswordChangeRequest {
        @NotNull(message = "error.password.password.required")
        @Size(min = 4, message = "error.password.password.length")
        private String password;
        @NotNull(message = "error.password.repeated.required")
        @Size(min = 4, message = "error.password.repeated.length")
        private String repeated;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getRepeated() {
            return repeated;
        }

        public void setRepeated(String repeated) {
            this.repeated = repeated;
        }
    }

    @RequestMapping("/admin/password")
    public String password(Model model) {
        fillLoggedInModelParameter(model);
        model.addAttribute("passwordChangeRequest", new PasswordChangeRequest());
        return "/admin/password";
    }

    @RequestMapping(value = "/admin/password", method = RequestMethod.POST)
    public String changePassword(@Valid PasswordChangeRequest passwordRequest, BindingResult result, Model model) {
        if (!result.hasFieldErrors("password")) {
            if (!passwordRequest.getPassword().equals(passwordRequest.getRepeated())) {
                result.addError(new FieldError("passwordChangeRequest", "password", "error.password.mustBeEqual"));
            }
        }

        if (!result.hasErrors()) {
            User user = getUser();
            user.setPassword(encodePassword(passwordRequest.password));
            userService.save(user);
        }

        fillLoggedInModelParameter(model);
        // This attribute has to have the same name like the
        model.addAttribute("passwordChangeRequest", passwordRequest);
        return "/admin/password";
    }

    @RequestMapping("/admin/user")
    public String show(@RequestParam(value = "id", required = false) String id, Model model) {
        if (!userService.isAdmin(getUser())) {
            throw new UnknownActionException("Unsupported operation selected");
        }
        fillLoggedInModelParameter(model);
        User toEdit;
        if (Strings.isNullOrEmpty(id)) {
            toEdit = new User();
        } else {
            toEdit = findById(id);
            if (toEdit == null) {
                throw new ElementNotFoundException("No element with id " + id);
            }
        }
        model.addAttribute("toEdit", toEdit);
        model.addAttribute("persons", allPersons());
        return "admin/user";
    }

    @RequestMapping(value = "/admin/user", method = RequestMethod.POST)
    public String edit(@ModelAttribute User toEdit, BindingResult result, Model model) {
        if (!userService.isAdmin(getUser())) {
            throw new UnknownActionException("Unsupported operation selected");
        }
        fillLoggedInModelParameter(model);

        User toStore;
        String userId = toEdit.getId();
        if (Strings.isNullOrEmpty(userId)) {
            // If there is no id, normally it is sent as empty string
            toEdit.setId(null);

            toStore = toEdit;
            toStore.setEnabled(true);
            toStore.setPassword(encodePassword(toStore.getPassword()));
            //toStore.setPassword(new BCryptPasswordEncoder().encode(toStore.getPassword()));
        } else {
            throw new UnknownActionException("Editing users is currently not supported");
        }

        OperationResult operationResult = userService.save(toStore);
        if (!operationResult.isSuccess()) {
            // TODO do better error handling here
            throw new UnknownActionException("Error while storing user");
        }

        return String.format("redirect:/admin/user?id=%s", toStore.getId());
    }

    private String encodePassword(String newPassword) {
        return webSecurityConfig.passwordEncoder().encode(newPassword);
    }

    private List<Person> allPersons() {
        return personService.findWithoutUsers();
    }

    @Override
    protected User findById(String id) {
        return userService.findById(id);
    }
}
