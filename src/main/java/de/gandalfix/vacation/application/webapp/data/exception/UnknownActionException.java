package de.gandalfix.vacation.application.webapp.data.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Unknown action")
public class UnknownActionException extends RuntimeException {

    public UnknownActionException(String message) {
        super(message);
    }
}
