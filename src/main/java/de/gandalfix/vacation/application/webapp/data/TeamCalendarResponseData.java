package de.gandalfix.vacation.application.webapp.data;

import de.gandalfix.vacation.domain.services.OperationResult;

import java.util.Map;
import java.util.Set;

public class TeamCalendarResponseData {

    public final OperationResult operationResult;
    /**
     * Caller Person.
     */
    public final PersonData person;
    /**
     * Group that should be displayed.
     */
    public final GroupData group;
    /**
     * Mapping person_id to person data.
     */
    public final Map<String, PersonData> idToPerson;
    /**
     * Mapping person_id to his vacation.
     */
    public final Map<String, Set<VacationData>> personToVacation;

    public TeamCalendarResponseData(OperationResult operationResult, PersonData person, GroupData group, Map<String, PersonData> idToPerson, Map<String, Set<VacationData>> personToVacation) {
        this.operationResult = operationResult;
        this.person = person;
        this.group = group;
        this.idToPerson = idToPerson;
        this.personToVacation = personToVacation;
    }
}
