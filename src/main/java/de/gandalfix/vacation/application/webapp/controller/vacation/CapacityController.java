package de.gandalfix.vacation.application.webapp.controller.vacation;

import de.gandalfix.vacation.application.webapp.controller.AbstractController;
import de.gandalfix.vacation.domain.data.entities.Group;
import de.gandalfix.vacation.domain.data.entities.User;
import de.gandalfix.vacation.domain.services.CapacityService;
import de.gandalfix.vacation.domain.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CapacityController extends AbstractController {

    @Autowired
    private CapacityService capacityService;

    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/capacity", method = RequestMethod.GET)
    public String capacityOverview(Model model) {
        User user = getUser();
        fillLoggedInModelParameter(model);
        List<Group> groups = loadVisibleGroups(user);
        model.addAttribute("groups", groups);
        LocalDate now = LocalDate.now();
        model.addAttribute("date_from", now.withDayOfMonth(1));
        model.addAttribute("date_to", now.withDayOfMonth(now.lengthOfMonth()));
        return "capacity";
    }

    private List<Group> loadVisibleGroups(User user) {
        List<Group> groups = new ArrayList<>(groupService.findGroupsForTeamCalendar(user));
        groups.sort((g1, g2) -> {
            int projectCompare = g1.getProject().getName().compareTo(g2.getProject().getName());
            if (projectCompare == 0) {
                return g1.getName().compareTo(g2.getName());
            }
            return projectCompare;
        });
        return groups;
    }

    @RequestMapping(value = "/capacity", method = RequestMethod.POST)
    public String capacityShow(
            @RequestParam("group")
            String groupId,
            @RequestParam("date_from")
            String fromString,
            @RequestParam("date_to")
            String toString, Model model) {
        User user = getUser();
        fillLoggedInModelParameter(model);

        if (!userService.canSeeTeamCalendar(user)) {
            // TODO some more errorhandling
            return "redirect:home";
        }

        Group group = groupService.findById(groupId);
        if (group == null) {
            // TODO some more errorhandling
            return "redirect:capacity";
        }
        List<Group> groups = loadVisibleGroups(user);
        model.addAttribute("groups", groups);

        LocalDate from = LocalDate.parse(fromString);
        LocalDate to = LocalDate.parse(toString);

        CapacityService.CapacityResult result = capacityService.calculateGroupCapacity(group, from, to, true);
        model.addAttribute("capacityResult", result);
        model.addAttribute("date_from", from);
        model.addAttribute("date_to", to);

        return "capacity";
    }
}
