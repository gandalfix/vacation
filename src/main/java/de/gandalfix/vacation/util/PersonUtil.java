package de.gandalfix.vacation.util;

import de.gandalfix.vacation.domain.data.entities.Person;

import javax.validation.constraints.NotNull;

public class PersonUtil {

    /**
     * Fills some attributes of a given person with the values from another one.
     * Esp. <em>not</em> filled are attributes from the base class.
     */
    public static void fillFromPerson(@NotNull Person personToFill, @NotNull Person personToFillFrom) {
        personToFill.setTitle(personToFillFrom.getTitle());
        personToFill.setFirstName(personToFillFrom.getFirstName());
        personToFill.setLastName(personToFillFrom.getLastName());
        personToFill.setCode(personToFillFrom.getCode());
        personToFill.setEmail(personToFillFrom.getEmail());
    }
}
