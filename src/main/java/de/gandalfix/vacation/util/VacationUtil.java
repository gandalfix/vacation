package de.gandalfix.vacation.util;

import de.gandalfix.vacation.application.webapp.data.VacationData;
import de.gandalfix.vacation.application.webapp.data.VacationPerYearData;
import de.gandalfix.vacation.domain.data.entities.*;

import java.time.LocalDate;
import java.util.*;

public class VacationUtil {

    public static Set<VacationData> vacationsToWebData(Collection<Vacation> vacations) {
        Set<VacationData> copy = new HashSet<>();
        vacations.forEach(entry -> copy.add(new VacationData(entry)));
        return copy;
    }

    public static List<VacationPerYearData> vacationAmountToWebData(Person person, Collection<VacationPerYear> amountPerYear) {
        List<VacationPerYearData> result = new ArrayList<>();
        Map<Integer, Integer> vacationLeftPerYear = getVacationLeftPerYear(person);
        amountPerYear.forEach(entry -> {
            int hoursLeft = -1;
            if (vacationLeftPerYear.containsKey(entry.getYear())) {
                hoursLeft = vacationLeftPerYear.get(entry.getYear());
            }
            result.add(new VacationPerYearData(entry, hoursLeft));
        });
        result.sort((o1, o2) -> o1.year - o2.year);
        return result;
    }

    /**
     * This function checks whether there is still enough vacation left for the given date.
     * If there is now vacation per year information for the year of the defect, this method will return true in any case.
     * @param person
     * @param vacationDate
     * @param type
     * @param status
     * @return
     */
    public static boolean isEnoughVacationLeft(Person person, LocalDate vacationDate, VacationType type, VacationStatus status) {
        Map<Integer, Integer> yearToCount = getVacationLeftPerYear(person);

        int newDateYear = vacationDate.getYear();
        if (yearToCount.containsKey(newDateYear)) {
            int currentCount = yearToCount.get(newDateYear);
            return currentCount >= type.getWorkingHours(8);
        }
        // If there is no information about max vacation amount, then everything is fine
        return true;
    }

    public static Map<Integer, Integer> getVacationLeftPerYear(Person person) {
        Map<Integer, Integer> yearToCount = new HashMap<>();
        person.getVacationPerYear()
                .forEach(vacationPerYear -> yearToCount.put(vacationPerYear.getYear(), vacationPerYear.getVacationHours()));

        person.getVacations()
                .forEach(vacation -> {
                    LocalDate date = vacation.getDate();
                    if (yearToCount.containsKey(date.getYear())) {
                        yearToCount.put(date.getYear(), yearToCount.get(date.getYear()) - (int)vacation.getType().getWorkingHours(8));
                    }
                });
        return yearToCount;
    }
}
