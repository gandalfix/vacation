package de.gandalfix.vacation.util;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtil {
    /**
     * Parses a date string that was posted into a LocalDate.
     * @param dateString not null
     * @return LocalDate instance
     */
    public static LocalDate parsePostDate(@NotNull String dateString) {
        return LocalDate.parse(dateString, DateTimeFormatter.ISO_DATE);
    }
}
