-- remove for real usage of application
DELETE FROM vacation;
DELETE FROM vacation_per_year;
DELETE FROM user_role;
DELETE FROM person_project;
DELETE FROM roles;
DELETE FROM users;
DELETE FROM person_group;
DELETE FROM person;
DELETE FROM groups;
DELETE FROM project;

insert into project (id, change_date, name) values ('1', now(), 'Projekt1');
insert into project (id, change_date, name) values ('2', now(), 'Brater');
insert into project (id, change_date, name) values ('3', now(), 'Projekt2');
insert into project (id, change_date, name) values ('4', now(), 'Projekt3');

insert into person (id, change_date, first_name, last_name, title) values ('1', now(), 'Jack', 'Bauer', null);
insert into person (id, change_date, first_name, last_name, title) values ('2', now(), 'Chloe', 'OBrian', null);
insert into person (id, change_date, first_name, last_name, title) values ('3', now(), 'Bilbo', 'Beutlin', null);
insert into person (id, change_date, first_name, last_name, title) values ('4', now(), 'Frodo', 'Beutlin', null);
insert into person (id, change_date, first_name, last_name, title) values ('5', now(), 'Spider', 'Man', null);

insert into groups (id, change_date, name, project_id) values ('1', now(), 'CTU', '1');
insert into groups (id, change_date, name, project_id) values ('3', now(), 'Terror', '1');
insert into groups (id, change_date, name, project_id) values ('2', now(), 'Fellowship', '2');
insert into groups (id, change_date, name, project_id) values ('4', now(), 'CTU2', '3');

insert into person_group (person_id, group_id) values ('1', '1');
insert into person_group (person_id, group_id) values ('2', '1');
insert into person_group (person_id, group_id) values ('2', '3');
insert into person_group (person_id, group_id) values ('2', '4');

insert into person_group (person_id, group_id) values ('3', '2');
insert into person_group (person_id, group_id) values ('4', '2');

insert into users (id, change_date, login, password, person_id, enabled, user_admin) values ('1', now(), 'jack', '$2a$10$EkgjRHIvl2A32nPD8oS/zOvit3GOz04l6DiRm6wD0uApR5.d8.3k2', '1', true, false);
insert into users (id, change_date, login, password, person_id, enabled, user_admin) values ('2', now(), 'chef', '$2a$10$EkgjRHIvl2A32nPD8oS/zOvit3GOz04l6DiRm6wD0uApR5.d8.3k2', '2', true, true);

insert into roles (id, change_date, name, project_id, can_grant, team_calendar, project_admin) values ('1', now(), 'PAN', '1', true, true, true);
insert into roles (id, change_date, name, project_id, can_grant, team_calendar, project_admin) values ('2', now(), 'PAN', '3', true, true, true);

insert into person_project (person_id, project_id) values ('1', '1');
insert into person_project (person_id, project_id) values ('1', '2');
insert into person_project (person_id, project_id) values ('2', '1');
insert into person_project (person_id, project_id) values ('3', '1');

insert into user_role (user_id, role_id) values ('2', '1');
insert into user_role (user_id, role_id) values ('2', '2');

insert into vacation_per_year (id, change_date, vacation_hours, year, person_id) values ('1', now(), 240, 2014, '1');
insert into vacation_per_year (id, change_date, vacation_hours, year, person_id) values ('2', now(), 256, 2015, '1');
insert into vacation_per_year (id, change_date, vacation_hours, year, person_id) values ('3', now(), 240, 2014, '2');
insert into vacation_per_year (id, change_date, vacation_hours, year, person_id) values ('4', now(), 240, 2015, '2');

insert into vacation (id, change_date, date, status, type, person_id) values ('1', now(), '2014-12-01', 'SUBMITTED', 'VACATION', '1');
insert into vacation (id, change_date, date, status, type, person_id) values ('2', now(), '2014-12-02', 'PLANNED', 'VACATION', '1');
insert into vacation (id, change_date, date, status, type, person_id) values ('3', now(), '2014-12-04', 'SUBMITTED', 'VACATION', '1');
insert into vacation (id, change_date, date, status, type, person_id) values ('4', now(), '2014-12-06', 'SUBMITTED', 'VACATION', '1');
