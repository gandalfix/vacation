var mapPersonIdToVacation = {};
var mapPersonIdToPerson = {};

var grant_vacation = function() {
    $.getJSON( "/vacationgrantdetails", function( data ) {
        console.log( "vacation information loaded" );
        convertAndStoreGrantResponseData(data);
        createGrantList();
    });
};

var convertAndStoreGrantResponseData = function (responseData) {
    // This is the person of the actual logged in user
    person = responseData["person"];
    mapPersonIdToPerson = responseData["subordinates"];

    var vacationsFromResponse = responseData["vacations"];

    for (var key in vacationsFromResponse) {
        if (vacationsFromResponse.hasOwnProperty(key)) {
            mapPersonIdToVacation[key] = convertVacationValues(vacationsFromResponse[key]);
        }
    }
};

/**
* Creates the list of vacation entries that have to be granted.
*/
var createGrantList = function() {
    var doc = document;
    var fragment = doc.createDocumentFragment();

    var tbody = doc.createElement("tbody");

    var personIds = [];
    for (var personId in mapPersonIdToPerson) {
        if (mapPersonIdToPerson.hasOwnProperty(personId)) {
            personIds.push(personId);
        }
    }

    for (var index = 0; index < personIds.length; index++) {
            var personId = personIds[index];
            var person = mapPersonIdToPerson[personId];
            var vacations = mapPersonIdToVacation[personId];

            var dates = getSortedVacationDates(vacations);

            for (var index2 = 0; index2 < dates.length; index2++) {
                var date = new Date(dates[index2]);
                var dayClass = buildDayClass(date);
                var vacationDate = vacations[dates[index2]];
                if (vacationDate.status && vacationDate.status == 'SUBMITTED') {
                    var row = doc.createElement("tr");

                    var submitColumn = doc.createElement("td");
                    var checkBox = document.createElement("INPUT");

                    var value = buildDayClass(date) + "_" + personId;

                    checkBox.setAttribute("type", "checkbox");
                    checkBox.setAttribute("name", "GRANT_VACATION");
                    checkBox.setAttribute("value", value);
                    submitColumn.appendChild(checkBox);
                    row.appendChild(submitColumn);

                    var personColumn = doc.createElement("td");
                    $(personColumn).html(person.fullName);
                    row.appendChild(personColumn);

                    var dateColumn = doc.createElement("td");
                    $(dateColumn).html(formatDate(date));
                    row.appendChild(dateColumn);

                    var typeColumn = doc.createElement("td");
                    $(typeColumn).html(vacationDate.type);
                    row.appendChild(typeColumn);

                    fragment.appendChild(row);

                    tbody.appendChild(fragment);
                }
            }
    }
    doc.getElementById("vacationsToGrantTable").appendChild(tbody);
};