// The information which persons are in the currently displayed group
var groupData = {};

// Mapping person_id -> person information
var personIdToPerson = [];

// Mapping person_id -> vacation information
var personToVacation = {};

var selectGroup = function(groupId) {

    if (!groupId) return;

    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    $.getJSON( "/calendar", {year: yearToDisplay}, function( data ) {
            console.log( "calender information loaded" );
            calendarDates = convertCalendarValues(data);
            $.ajax({
                type: "POST",
                url: "/teamcalendar",
                beforeSend: function (request)
                {
                    request.setRequestHeader(header, token);
                },
                data: { "groupId": groupId, "yearToDisplay" : yearToDisplay }
            })
            .done(function( data ) {
                convertAndStoreCalendarResponseData(data);
                createTeamCalendar();
                console.log("response " + data);
            })
            .fail(function( data ) {
                var operationResult = data["responseJSON"]["operationResult"];
                showErrorOnOperationResult(operationResult);
                console.log("error: response " + data);
            });
    });
};

var createTeamCalendar = function() {

    $("#team_calendar_wrapper").remove();

    var doc = document;
    var fragment = doc.createDocumentFragment();

    var teamCalendarWrapper = doc.createElement("div");
    teamCalendarWrapper.id = "team_calendar_wrapper";
    fragment.appendChild(teamCalendarWrapper);

    // For each month there is an individual calendar "row"
    for (var month = 0; month < 12; month++) {
        createTeamCalendarMonth(month, yearToDisplay, teamCalendarWrapper);
    }

    doc.getElementById("team_calendar").appendChild(fragment);
    // Keep if I want to try rotating the month name again
    //$(document).ready(function () {
    //    $('.rotate').css('height', $('.rotate').width());
    //});
    updateCalendarDate();
};

/**
 *
 <div class=" panel-default">
 <div class="panel-heading">
 <h3 class="panel-title">Panel title</h3>
 </div>
 <div class="panel-body">
 Panel content
 </div>
 </div>
 */
var createTeamCalendarMonth = function(month, year, fragment) {
    var doc = document;

    var teamSize = arraySize(personIdToPerson);

    var currentDate = new Date(year, month, 1);

    var monthWrapper = doc.createElement("div");
    $(monthWrapper)
        .addClass("team_calendar_month_wrapper")
        .addClass("panel").addClass("panel-primary")
    ;
    fragment.appendChild(monthWrapper);

    var tableLabel = doc.createElement("div");
    monthWrapper.appendChild(tableLabel);
    $(tableLabel)
        .addClass("team_calendar_header_column")
        .addClass("rotate")
        .addClass("panel-heading")
    ;

    var headerText = doc.createElement("span");
    $(headerText)
        .addClass("panel-title")
        .html(getMonthName(currentDate, language));
    tableLabel.appendChild(headerText);

    var monthTableWrapper = doc.createElement("div");
    $(monthTableWrapper)
        .addClass("panel-body");

    var monthTable = doc.createElement("table");
    $(monthTable).addClass("team_calendar_month");

    monthTableWrapper.appendChild(monthTable);
    monthWrapper.appendChild(monthTableWrapper);


    var monthTableBody = doc.createElement("tbody");
    monthTable.appendChild(monthTableBody);

    //=========================================
    // First do the "header" row, containing the column header and day number
    //=========================================
    var firstRow = doc.createElement("tr");
    $(firstRow).addClass("team_calendar_header_row")
               .addClass("team_calendar_row");
    monthTableBody.appendChild(firstRow);

    var headerColumn = doc.createElement("td");
    // Not added by intention as rotating sucks and does not work easily
//    firstRow.appendChild(headerColumn);
    $(headerColumn)
        .attr("rowspan", teamSize + 1)
        .addClass("team_calendar_header_column")
        .html(getMonthName(currentDate, language));

    var nameColumn = doc.createElement("td");
    firstRow.appendChild(nameColumn);
    $(nameColumn).html("Person") // TODO has to be localized
                 .addClass("team_calendar_person_column");

    while (currentDate.getMonth() == month) {
        var dayClass = buildDayClass(currentDate);

        var dayColumn = doc.createElement("td");
        firstRow.appendChild(dayColumn);
        $(dayColumn).html(currentDate.getDate());
        $(dayColumn)
            .addClass("calendar_type")
            .addClass(dayClass);
        if (currentDate.getDay() == 0 || currentDate.getDay() == 6) {
            $(dayColumn).addClass("weekend");
        }

        currentDate.setDate(currentDate.getDate()+1);
    }

    //=========================================
    // Now the same thing again for the persons
    //=========================================

    for (var i = 0; i < personIdToPerson.length; i++) {
            currentDate = new Date(year, month, 1);

            var person = personIdToPerson[i];
            var vacations = personToVacation[person[0]];

            var personRow = doc.createElement("tr");
            $(personRow).addClass("team_calendar_person_row")
                        .addClass("team_calendar_row");
            monthTableBody.appendChild(personRow);

            var headerColumn = doc.createElement("td");
            $(headerColumn).addClass("team_calendar_header_column");
            // If the "header" column above is added, this must not be added, as otherwise the rowspan breaks
            //personRow.appendChild(headerColumn);

            var nameColumn = doc.createElement("td");
            personRow.appendChild(nameColumn);
            $(nameColumn).html(person[1].fullName)
                         .addClass("team_calendar_person_column");

            while (currentDate.getMonth() == month) {
                var dayClass = buildDayClass(currentDate);

                var dayColumn = doc.createElement("td");
                personRow.appendChild(dayColumn);
                $(dayColumn).addClass("calendar_type").addClass(dayClass);
                if (currentDate.getDay() == 0 || currentDate.getDay() == 6) {
                    $(dayColumn).addClass("weekend");
                }

                if (vacations) {
                    var currentVaction = vacations[currentDate];
                    if (currentVaction) {
                        if (currentVaction.type) {
                            $(dayColumn).addClass(currentVaction.type).html(VACATION_TYPES[currentVaction.type].abbreviation);
                        }
                    }
                }

                currentDate.setDate(currentDate.getDate()+1);
            }
    }
};

function convertAndSortIdToPersonResponseData(responseData) {
    var personIdToPersonFromResponse = responseData["idToPerson"];

    personIdToPerson = [];

    for (var key in personIdToPersonFromResponse) {
        if (personIdToPersonFromResponse.hasOwnProperty(key)) {
            personIdToPerson.push([key, personIdToPersonFromResponse[key]]);
        }
    }

    personIdToPerson.sort(function (a, b) {
        a = a[1].fullName;
        b = b[1].fullName;

        return a < b ? -1 : (a > b ? 1 : 0);
    });
}

var convertAndStoreCalendarResponseData = function(responseData) {
    person = responseData["person"];
    groupData = responseData["group"];
    convertAndSortIdToPersonResponseData(responseData);
    var vacationsFromResponse = responseData["personToVacation"];

    for (var key in vacationsFromResponse) {
        if (vacationsFromResponse.hasOwnProperty(key)) {
            personToVacation[key] = convertVacationValues(vacationsFromResponse[key]);
        }
    }
};