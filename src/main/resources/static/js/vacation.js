var currentVacationPerYear = {vacationHours: 0, vacationHoursLeft: 0};

var vacation = function () {
$.getJSON( "/calendar", {year: yearToDisplay}, function( data ) {
        console.log( "calender information loaded" );
        calendarDates = convertCalendarValues(data);
        $.getJSON( "/vacationDetail", function( data ) {
            console.log( "vacation information loaded" );
            convertAndStoreResponseData(data);
            createYearSelector();
            createCalendar();
        })
        .fail(function() {
            console.log( "error while retrieving vacation detail data" );
        });
    })
    .fail(function() {
        console.log( "error while retrieving calendar data" );
    });
};

/**
* Cycles the various vacation types so that clicking always returns the next one.
*/
var cycleVacationType = function(typeAbbr) {

    for (var key in VACATION_TYPES) {
        if (VACATION_TYPES.hasOwnProperty(key)) {
            if (VACATION_TYPES[key].abbreviation == typeAbbr) {
                return VACATION_TYPES[VACATION_TYPES[key].followup];
            }
        }
    }
    return VACATION_TYPES["NO_VACATION"];
};

/**
* Updates the calendar with the information from vacationDates
*/
var updateVacations = function() {
    console.log("updating vacations started");
    // first remove all marked entries
    for (var key in VACATION_TYPES) {
        if (VACATION_TYPES.hasOwnProperty(key)) {
              $("." + key)
                .removeClass(key)
                .html("&nbsp;");
        }
    }
    for (var key in VACATION_STATUS) {
        if (VACATION_STATUS.hasOwnProperty(key)) {
              $("." + key)
                .removeClass(key)
                .removeClass(VACATION_STATUS[key].labelClass)
                .html("");
        }
    }

    for (var key in vacationDates) {
      if (vacationDates.hasOwnProperty(key)) {
        var date = new Date(key);
        var dayClass = buildDayClass(date);
        var vacationDate = vacationDates[key];
        if (vacationDate.type) {
            $("." + dayClass + ".calendar_type")
                .addClass(vacationDates[date].type)
                .addClass(VACATION_TYPES[vacationDate.type].labelClass)
                .html(VACATION_TYPES[vacationDate.type].abbreviation);
        }
        if (vacationDate.status) {
            $("." + dayClass + ".calendar_status")
                .addClass(vacationDates[date].status)
                .addClass("label")
                .addClass(VACATION_STATUS[vacationDate.status].labelClass)
                .html(VACATION_STATUS[vacationDate.status].abbreviation);
        }
      }
    }
    console.log("updating vacations completed")
};

/**
* Build a month of the calendar.
* Iterate over the month and colorize:
* * weekends
*/
var createCalendarMonth = function(month, year) {

    console.log( "calendar drawing for " + month + "/" + year + " started" );

    var doc = document;
    var fragment = doc.createDocumentFragment();

    var currentDate = new Date(year, month, 1);

    // Create the monthly header
    var monthHeader = doc.createElement("tr");

    var monthTitle = doc.createElement("th");
    monthTitle.setAttribute("colSpan", 3);
    monthTitle.innerHTML = getMonthName(currentDate, language);
    monthHeader.appendChild(monthTitle);

    fragment.appendChild(monthHeader);

    while (currentDate.getMonth() == month) {
        var dayClass = buildDayClass(currentDate);

        var dayRow = doc.createElement("tr");

        //=============================
        // Handle day column - start
        //=============================
        var dayTd = doc.createElement("td");
        var day = doc.createElement("span");
        day.innerHTML = currentDate.getDate();
        $(day).addClass("calendar_day").addClass(dayClass);
        if (currentDate.getDay() == 0 || currentDate.getDay() == 6) {
            $(day).addClass("weekend");
        }
        dayTd.appendChild(day);
        dayRow.appendChild(dayTd);
        //=============================
        // Handle day column - end
        //=============================

        //=============================
        // Handle type column - start
        //=============================
        var vacationTypeTd = doc.createElement("td");
        var vacationType = doc.createElement("span");
        $(vacationType)
            .addClass("calendar_type")
            .addClass(dayClass)
            .html("&nbsp;");
        if (currentDate.getDay() == 0 || currentDate.getDay() == 6) {
            $(vacationType).addClass("weekend");
        }
        vacationTypeTd.appendChild(vacationType);
        dayRow.appendChild(vacationTypeTd);
        //=============================
        // Handle type column - end
        //=============================

        //=============================
        // Handle status column - start
        //=============================
        var vacationStatusTd = doc.createElement("td");
        var vacationStatus = doc.createElement("span");

        $(vacationStatus)
            .addClass("calendar_status")
            .addClass(dayClass)
            .html("&nbsp;");
        if (currentDate.getDay() == 0 || currentDate.getDay() == 6) {
            $(vacationStatus).addClass("weekend");
        }
        vacationStatusTd.appendChild(vacationStatus);
        dayRow.appendChild(vacationStatusTd);
        //=============================
        // Handle status column - end
        //=============================

        //does not trigger reflow
        fragment.appendChild(dayRow);

        currentDate.setDate(currentDate.getDate()+1);
    }

    // TableContainer is the column of the container table for the current month
    var tableContainer = doc.createElement("td");
    // monthTable holds actually the whole month
    var monthTable = doc.createElement("table");
    monthTable.className = "calendar_month";
    monthTable.appendChild(fragment);

    tableContainer.appendChild(monthTable);
    doc.getElementById("calendar_wrapper_row").appendChild(tableContainer);
    console.log( "calendar drawing for " + month + "/" + year + " completed" );
};

/**
* Build the whole calendar for a complete year.
*/
var createCalendar = function() {
    console.log( "calendar drawing started" );

    var doc = document;
    var fragment = doc.createDocumentFragment();

    var table = doc.createElement("table");
    table.id = "calendar_wrapper";

    var wrapperRow = doc.createElement("tr");
    wrapperRow.id = "calendar_wrapper_row";
    table.appendChild(wrapperRow);

    table.appendChild(fragment);

    // #calendar is actually the div within the HTML
    doc.getElementById("calendar").appendChild(table);

    for (i = 0; i < 12; i++) {
        createCalendarMonth(i, yearToDisplay);
    }

    // Initial fill of dates already known
    updateCalendarDate();
    updateVacations();
    updateHolidayProgress();

    addClickHandler(
        $( ".calendar_type" ),
            function(event) {
                event.preventDefault();
                postVacationChange(this);
           });
    removeClickHandler($( ".weekend" ));
    removeClickHandler($(".bank_holiday"));

    console.log( "calendar drawing completed" );
};

/**
 * Creates the "progressbar" for the number of vacation days.
 */
var updateHolidayProgress = function () {

    setVacationPerYear(yearToDisplay);

    var holidayProgress = $("#vacationProgress");

    holidayProgress.empty();

    var daysPerYear = currentVacationPerYear.vacationHours / 8;
    var daysLeft = currentVacationPerYear.vacationHoursLeft / 8;

    var width = 100 / daysPerYear;

    var daysPerYearRounded = daysPerYear - (daysPerYear % 1);
    var daysLeftModulo = daysLeft % 1;
    var daysLeftRounded = daysLeft - (daysLeftModulo);

    var usedUpVacationForCurrentYear = daysPerYearRounded - daysLeftRounded;

    for (var i = 0; i < daysLeftRounded; i++ ) {
        var span = drawDayProgressEntry("progressDayUnused", width);
        holidayProgress.append(span);
    }

    if (daysLeftModulo > 0) {
        var span = drawDayProgressEntry("progressDayUnused", width/2);
        holidayProgress.append(span);
        var span = drawDayProgressEntry("progressDayUsed", width/2);
        holidayProgress.append(span);
        usedUpVacationForCurrentYear--;
    }

    for (var i = 0; i < usedUpVacationForCurrentYear; i++) {
        var span = drawDayProgressEntry("progressDayUsed", width);
        holidayProgress.append(span);
    }
};

var drawDayProgressEntry = function (additionalClass, width) {
    var span = document.createElement("span");
    $(span)
        .addClass("progressDay")
        .addClass(additionalClass)
        .css("width", width + "%")
        .html("&nbsp;");
    return span;
};

/**
* Removes and re-creates the calendar.
*/
var recreateCalendar = function() {
    $("#calendar_wrapper").remove();
    createCalendar();
};

/**
* Creates the year selectors for year for which the user has a number of vacation days.
*/
var createYearSelector = function() {
    console.log( "year selector drawing started" );
    var doc = document;
    var fragment = doc.createDocumentFragment();

    for (var index = 0; index < vacationPerYear.length; index++) {
        var year = vacationPerYear[index].year;
        var yearSelectContainer = doc.createElement("li");
        var yearSelect = doc.createElement("a");
        $(yearSelect)
            .addClass("year_select")
            .addClass("year_" + year)
            .attr("href", "#")
            .attr("role", "presentation")
            .html(year)
            .click(function(event) {
                event.preventDefault();
                $(".year_select").parent().removeClass("active");
                $(this).parent().addClass("active");
                yearToDisplay = this.innerHTML;
                $.getJSON( "/calendar", {year: yearToDisplay}, function( data ) {
                        console.log( "calender information loaded" );
                        calendarDates = convertCalendarValues(data);
                        recreateCalendar();
                        });
            })
            .css( 'cursor', 'pointer' );
        yearSelectContainer.appendChild(yearSelect);
        fragment.appendChild(yearSelectContainer);

        doc.getElementById("available_years").appendChild(fragment);
    }

    $(".year_" + yearToDisplay).parent().addClass("active");
    console.log( "year selector drawing completed" );
};

var setVacationPerYear = function (year) {
    currentVacationPerYear = {
        vacationHours: 0,
        vacationHoursLeft: 0
    };
    for (var index = 0; index < vacationPerYear.length; index++) {
        if (year == vacationPerYear[index].year) {
            currentVacationPerYear = vacationPerYear[index];
            break;
        }
    }
};

var postVacationChange = function(element) {
    console.log(element);
    console.log(element.innerHTML);

    var day = "";
    var type = cycleVacationType(element.innerHTML);

    var classes = element.className.split(" ");
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    for	(var i = 0; i < classes.length; i++) {
        var curclass = classes[i];
        if (curclass && curclass.slice(0, "day_".length) == "day_") {
            day = curclass.substring("day_".length, curclass.length);
            break;
        }
    }
    if (day != "") {
        $.ajax({
            type: "POST",
            url: "/vacationUpdate",
            beforeSend: function (request)
            {
                request.setRequestHeader(header, token);
            },
            data: { "date": day, "type": type.abbreviation }
        })
        .done(function( data ) {
            convertAndStoreResponseData(data);
            updateVacations();
            updateHolidayProgress();
            console.log("response " + data);
        })
        .fail(function( data ) {
            var operationResult = data["responseJSON"]["operationResult"];
            showErrorOnOperationResult(operationResult);
            console.log("error: response " + data);
        });
    }
};