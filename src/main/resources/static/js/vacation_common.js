/**
 * This contains the general date information.
 * Like bank holidays
 */
var calendarDates = "";

/**
 * This contains the vacation for the logged in user.
 */
var vacationDates = "";

/**
 * The year that is currently displayed if a calendar is shown
 */
var yearToDisplay = new Date().getFullYear();

var vacationPerYear = [];

var language = $("meta[name='_language']").attr("content");

var person = "";

var VACATION_TYPES = {
    "HALF_DAY_VACATION": {
        name: "HALF_DAY_VACATION",
        abbreviation: "H",
        followup: "VACATION"
    },
    "VACATION": {
                name: "VACATION",
                abbreviation: "V",
                followup: "TRAINING"
              },
    "TRAINING": {
                name: "TRAINING",
                abbreviation: "T",
                followup: "SPECIAL_VACATION"
              },
    "SPECIAL_VACATION": {
                name: "SPECIAL_VACATION",
                abbreviation: "S",
                followup: "OVER_HOURS"
              },
    "OVER_HOURS": {
                name: "OVER_HOURS",
                abbreviation: "O",
                followup: "PROJECT_OFF"
              },
    "PROJECT_OFF": {
                name: "PROJECT_OFF",
                abbreviation: "P",
                followup: "NO_VACATION"
              },
    "NO_VACATION": {
                name: "NO_VACATION",
                abbreviation: "&nbsp;",
                followup: "HALF_DAY_VACATION"
              }
};

var VACATION_STATUS = {
    "PLANNED" : {
        abbreviation: "?",
        labelClass: "label-default"
        },
    "SUBMITTED" : {
        abbreviation: "&#187;",
        labelClass: "label-primary"
        },
    "GRANTED" : {
        abbreviation: "&#10003;",
        labelClass: "label-success"
        }
};

/**
* Formats a date as ISO YYYY-MM-DD.
*/
var formatDate = function(date) {
    var result = "" + date.getFullYear() + "-";
    var month = date.getMonth()+1;
    if (month < 10) result += "0";
    result += month + "-";
    if (date.getDate() < 10) result += "0";
    result += date.getDate();
    return result;
};

var showErrorOnOperationResult = function(operationResult) {
    var errorMessage = "Error while performing operation";
    if (operationResult && operationResult["message"]) {
        errorMessage = operationResult["message"];
    }
    $( "#message" ).html(errorMessage).dialog();
};

/**
* Get the name of a month in the given language
*/
var getMonthName = function(date, lang) {
    lang = lang && (lang in localeDateNames) ? lang : 'en';
    return localeDateNames[lang].month_names[date.getMonth()];
};

/**
* Get the short name of a month in the given language
*/
var getMonthNameShort = function(date, lang) {
    lang = lang && (lang in localeDateNames) ? lang : 'en';
    return localeDateNames[lang].month_names_short[date.getMonth()];
};

/**
* Localized month names in order to provide (theoretically) translated GUI ;-)
*/
localeDateNames = {
    en: {
       month_names: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
       month_names_short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    de: {
       month_names: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
       month_names_short: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez']
    }
};

var convertAndStoreResponseData = function(responseData) {
    vacationDates = convertVacationValues(responseData["vacations"]);
    person = responseData["person"];
    vacationPerYear = responseData["vacationPerYear"];
};

var buildDayClass = function(date) {
    return "day_" + formatDate(date);
};

/**
* Converts the JSON LocalDate of vacations into an associated array.
* <date> -> status, type;
*/
var convertVacationValues = function (vacationData) {
    var output = {};

    for	(var index = 0; index < vacationData.length; index++) {
        var current = vacationData[index];
        var year = current.date.year;
        var month = current.date.monthValue;
        var day = current.date.dayOfMonth;

        var status = current.status;
        var type = current.type;

        output[new Date(year, month-1, day)] = {status:status, type:type};
    }

    return output;
};

/**
* Converts the JSON LocalDate of calenderDate into an associated array.
* <date> -> bankHoliday, halfDay;
*/
var convertCalendarValues = function (calendarData) {
    var output = {};

    for	(var index = 0; index < calendarData.length; index++) {
        var current = calendarData[index];
        var year = current.date.year;
        var month = current.date.monthValue;
        var day = current.date.dayOfMonth;

        var bankHoliday = current.bankHoliday;
        var halfDay = current.halfDay;

        output[new Date(year, month-1, day)] = {bankHoliday:bankHoliday, halfDay:halfDay};
    }

    return output;
};

/**
* Retrieves the keys of the date -> vacation mapping in a ordered manner.
*/
var getSortedVacationDates = function(vacations) {
    var dates = [];
    for (var key in vacations) {
       if (vacations.hasOwnProperty(key)) {
            dates.push(key);
        }
    }

    dates.sort(function(a,b){
        return new Date(a) - new Date(b);
    });
    return dates;
};

var arraySize = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

/**
* Updates the calendar with the information from calendarDates.
*/
var updateCalendarDate = function() {
    console.log("updating calendar dates started");
    // first remove all marked entries
    $(".bank_holiday").removeClass("bank_holiday");
    $(".half_day").removeClass("half_day");

    for (var key in calendarDates) {
      if (calendarDates.hasOwnProperty(key)) {
        var date = new Date(key);
        var dayClass = buildDayClass(date);
        var calendarDate = calendarDates[key];
        if (calendarDate.bankHoliday) $("." + dayClass ).addClass("bank_holiday");
        if (calendarDate.halfDay) $("." + dayClass ).addClass("half_day");
      }
    }
    console.log("updating calendar dates completed")
};

/**
 * Adds the given function to as click handler to an element and sets the cursor.
 */
var addClickHandler = function (element, func) {
    element.click(func)
           .css('cursor', 'pointer');
};

/**
 * Removes any click handler and resets the cursor.
 */
var removeClickHandler = function (element) {
    element.unbind('click')
           .css('cursor', 'auto');
};