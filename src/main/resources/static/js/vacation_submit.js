

var submit_vacation = function() {
    $.getJSON( "/vacationDetail", function( data ) {
        console.log( "vacation information loaded" );
        convertAndStoreResponseData(data);
        createSubmitList();
    });
};

var createSubmitList = function() {
    var doc = document;
    var fragment = doc.createDocumentFragment();

    var tbody = doc.createElement("tbody");

    var keys =getSortedVacationDates(vacationDates);

    for (var index = 0; index < keys.length; index++) {
            var key = keys[index];
            var date = new Date(key);
            var dayClass = buildDayClass(date);
            var vacationDate = vacationDates[key];
            if (vacationDate.status && vacationDate.status == 'PLANNED') {
                var row = doc.createElement("tr");

                var submitColumn = doc.createElement("td");
                var checkBox = document.createElement("INPUT");
                checkBox.setAttribute("type", "checkbox");
                checkBox.setAttribute("name", "SUBMIT_VACATION");
                checkBox.setAttribute("value", buildDayClass(date));
                submitColumn.appendChild(checkBox);
                row.appendChild(submitColumn);

                var dateColumn = doc.createElement("td");
                $(dateColumn).html(formatDate(date));
                row.appendChild(dateColumn);

                var typeColumn = doc.createElement("td");
                $(typeColumn).html(vacationDate.type);
                row.appendChild(typeColumn);

                fragment.appendChild(row);

                tbody.appendChild(fragment);
            }
    }
    doc.getElementById("vacationsToSubmitTable").appendChild(tbody);
};