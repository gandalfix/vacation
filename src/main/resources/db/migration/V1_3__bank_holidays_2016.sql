insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-01-01'::text)::cstring) , now() ,'2016-01-01', 'Neujahr', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-01-06'::text)::cstring) , now() ,'2016-01-06', 'Heilige Drei Könige', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-03-25'::text)::cstring) , now() ,'2016-03-25', 'Karfreitag', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-03-28'::text)::cstring) , now() ,'2016-03-28', 'Ostermontag', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-05-01'::text)::cstring) , now() ,'2016-05-01', 'Maifeiertag', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-05-05'::text)::cstring) , now() ,'2016-05-05', 'Christi Himmelfahrt', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-05-16'::text)::cstring) , now() ,'2016-05-16', 'Pfingstmontag', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-05-26'::text)::cstring) , now() ,'2016-05-26', 'Fronleichnam', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-08-15'::text)::cstring) , now() ,'2016-08-15', 'Mariä Himmelfahrt', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-10-03'::text)::cstring) , now() ,'2016-10-03', 'Tag der Deutschen Einheit', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-11-01'::text)::cstring) , now() ,'2016-11-01', 'Allerheiligen', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-12-25'::text)::cstring) , now() ,'2016-12-25', '1. Weihnachtstag', true, false);
insert into calendar_date (id, change_date, date, name, bank_Holiday, half_Day) values (uuid_in(md5('2016-12-26'::text)::cstring) , now() ,'2016-12-26', '2. Weihnachtstag', true, false);
